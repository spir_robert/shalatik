﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using IrcClass;
using System.Security.Cryptography;
using System.Data.SQLite;

namespace shalatik
{
    partial class Shalatik
    {
        private const string userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36 OPR/55.0.2994.61";
        private  IrcClient irc = new  IrcClient();       
        DatabaseConnect dc;
        Settings settings;
        Random generator;
        Thread qogthread, regenthread, respawnthread, cooldownthread;
        List<string> ignorelist;
        DateTime lastlearned, lastdanced;
        internal bool running = true;
        public bool fefo { get; set; }

        public Shalatik(Settings set)
        {
            irc.ActiveChannelSyncing = true;
            irc.SendDelay = 100;
            irc.AutoNickHandling = true;
            irc.OnConnected += OnConnected;
            irc.OnChannelMessage += OnChannelMessage;
            irc.OnQueryMessage += OnQueryMessage;
            irc.OnRawMessage += OnRawMessage;
            irc.OnJoin += OnJoin;
            //irc.OnDisconnected += OnDisconnected;
            irc.OnKick += OnKick;
            irc.OnNickChange += OnNickChange;
            irc.OnNames += OnNames;
            irc.OnDeop += irc_OnDeop;
            irc.AutoRejoin = true;
            irc.OnPart += irc_OnPart;
            irc.OnQuit += irc_OnQuit;
            irc.Encoding = System.Text.Encoding.UTF8;
            settings = set;
            generator = new Random();
            ignorelist = new List<string>();
            lastlearned = new DateTime(1900, 1, 1);
            lastdanced = DateTime.Now.AddHours(generator.Next(24) + 24).AddMinutes(generator.Next(61));
            fefo = false;
            dc = new DatabaseConnect(settings.con, settings.con2);
            qogthread = new Thread(QogThread);
            qogthread.Start(dc);
        }

        void irc_OnQuit(object sender, QuitEventArgs e)
        {
            settings.noobs[settings.noobs.IndexOf(e.Who)].level = 0;
        }

        void irc_OnPart(object sender, PartEventArgs e)
        {
            settings.noobs[settings.noobs.IndexOf(e.Who)].level = 0;
        }

        void irc_OnDeop(object sender, DeopEventArgs e)
        {
            if (e.Whom.Equals("doug", StringComparison.OrdinalIgnoreCase)&&!e.Who.Equals(settings.nick,StringComparison.OrdinalIgnoreCase))
            {
                irc.Deop(e.Channel, e.Who);
                irc.Op(e.Channel, "Doug");
            }
        }

        public bool GetStav()
        {
            return irc.IsConnected;
        }
        private void OnDisconnected(object sender, EventArgs ee)
        {
            //regenthread.Abort();
            //respawnthread.Abort();
            //qogthread.Abort();
            //cooldownthread.Abort();
        }
        void OnNickChange(object sender, NickChangeEventArgs e)
        {
            string str = e.OldNickname;
            if (settings.noobs.Contains(str))
            {
                settings.noobs[settings.noobs.IndexOf(str)].name = e.NewNickname;
            }
            else if (e.NewNickname != e.Data.Irc.Nickname)
            {
                settings.noobs.Add(e.NewNickname);
            }
        }

        void OnNames(object sender, NamesEventArgs e)
        {
            foreach (string str in e.UserList)
            {
                if (str != e.Data.Irc.Nickname && !settings.noobs.Contains(str))
                {
                    if (str.StartsWith("@")) settings.noobs.Add(str.Remove(0,1));
                    else if (str.StartsWith("+")) settings.noobs.Add(str.Remove(0, 1)); 
                    else settings.noobs.Add(str);
                }
            }
        }
        void PrivMsg(string chan, string message)
        {
            string musug = message;
            while(musug.Length>0)
            {
                int i = Math.Min(300,musug.Length);
                if(i==musug.Length)
                {
                    irc.RfcPrivmsg(chan, musug.Trim());
                    break;
                }
                while (musug[i] != ' ') i--;
                string mes = musug.Substring(0, i);
                irc.RfcPrivmsg(chan, mes.Trim());
                musug = musug.Remove(0, i);
                Thread.Sleep(500);
            }
        }
        void OnJoin(object senderr, JoinEventArgs e)
        {
            string str = e.Data.Nick;
            if (str != e.Data.Irc.Nickname && !settings.noobs.Contains(str))
            {
                settings.noobs.Add(str);
            }
            string channel = e.Data.Channel;
            string sender = e.Data.Nick;
            /*lock (dc)
            {
                if (!sender.Equals(settings.nick))
                {
                    String query = String.Format("SELECT greeting from irc_greetings where LOWER(nick) = '{0}' ORDER BY RANDOM() LIMIT 1", sender.ToLower());
                    using (SQLiteDataReader rs = dc.sendQuery(query))
                    {
                        String ar = "";
                        while (rs.Read())
                        {
                            ar = rs["greeting"].ToString();
                        }
                        rs.Close();
                        if (!string.IsNullOrEmpty(ar))
                        {
                            PrivMsg(channel, ar);
                        }
                        else
                        {
                            if (generator.Next(100) < 20)
                            {
                                PrivMsg(channel, String.Format("hey {0}, JESUS man!", sender));
                            }
                        }
                    }
                }
            }*/
        }
        public Shalatik()
        {
            throw new Exception("to sa neda!!");
        }
         

        void OnRawMessage(object sender, IrcEventArgs e)
        {
            Console.WriteLine(String.Format("{0} {1}", DateTime.Now.ToLocalTime(), e.Data.RawMessage));
        }

        public void Connect()
        {
            CultureInfo ci = new CultureInfo("en-us");
            Thread.CurrentThread.CurrentCulture = ci;
            string serv="";
            /*regenthread = new Thread(RegenThread);
            regenthread.Start(dc);
            respawnthread = new Thread(RespawnThread);
            respawnthread.Start(dc);
            cooldownthread = new Thread(CooldownThread);
            cooldownthread.Start();*/
            try
            {
                serv=settings.server.Dequeue();
                settings.server.Enqueue(serv);
                irc.Connect(serv, settings.port);
            }
            catch (Exception e)
            {
                Console.Write(String.Format("Failed to connect: {0}\n{1}", e.Message, e.StackTrace));
                settings.server.Enqueue(serv);
            }
        }

        void OnConnected(object sender, EventArgs e)
        {
            irc.Login(new string[] {settings.nick,"bluglafnuf"}, "CSharp Bot", 0, "NEU_MANN");
            irc.RfcJoin(settings.chan);
            irc.Listen();
        }


        void OnChannelMessage(object senderr, IrcEventArgs args)
        {
            string message = args.Data.Message;
            string channel = args.Data.Channel;
            string sender = args.Data.Nick;
            if (ignorelist.Contains(sender)) return;
            if(args.Data.Nick.Equals("idlerpg"))return;
            lock (dc)
            {
                if (message.StartsWith("!addquote ", StringComparison.OrdinalIgnoreCase))
                {
                    Message_AddQuote(message, channel);
                }
                else if (message.StartsWith("!rose ", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Rose(message, channel, sender);
                }
                else if (message.StartsWith("!penis ", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Penis(message, channel, sender);
                }
                else if (message.StartsWith("?? ", StringComparison.OrdinalIgnoreCase))
                {
                    Message_QQ(message, channel);
                }
                else if (message.StartsWith("!ignore ", StringComparison.OrdinalIgnoreCase))
                {
                    if (settings.noobs[settings.noobs.IndexOf(sender)].level < 5)
                    {
                        PrivMsg(channel, "insufficient kredencials!");
                    }
                    else
                    {
                        ignorelist.Add(message.Split(' ')[1]);
                    }
                } 
                else if (message.StartsWith("!unignore ", StringComparison.OrdinalIgnoreCase))
                {
                    if (settings.noobs[settings.noobs.IndexOf(sender)].level < 5)
                    {
                        PrivMsg(channel, "insufficient kredencials!");
                    }
                    else
                    {
                        ignorelist.Remove(message.Split(' ')[1]);
                    }
                }
                else if (message.StartsWith("!ping", StringComparison.OrdinalIgnoreCase))
                {
                    PrivMsg(channel, String.Format("pong {0}, pong!!", sender));
                }
                //vypis spelllov
                /*else if (message.StartsWith("!spells", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Spells(message, channel);
                }*/
                else if (message.StartsWith("!uptime", StringComparison.OrdinalIgnoreCase))
                {
                    PrivMsg(channel, DateTime.Now.Subtract(Process.GetCurrentProcess().StartTime).ToString());
                }
                //qouty
                else if (message.StartsWith("!quotefrom", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Quotefrom(message, channel);
                }
                else if (message.StartsWith("!quotecount", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Quotecount(message, channel);
                }
                else if (message.StartsWith("!quote", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Quote(message, channel);
                }
                else if (message.StartsWith("!kick", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Kick(message, channel, sender);
                }
                // LEARNZOR
                else if (message.StartsWith("!learn ", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Learn(message, channel, sender);
                }
                else if (message.StartsWith("!google ", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        PrivMsg(channel, Googluj(message.Remove(0, 8)));
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        PrivMsg(channel, "skus este raz, trochec sa to dojebabralo");
                    }
                }
                else if (message.StartsWith("!bing ", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        PrivMsg(channel, Binguj(message.Remove(0, 6)));
                    }
                    catch
                    {
                        PrivMsg(channel, "skus este raz, trochec sa to dojebabralo");
                    }
                }
                else if (message.StartsWith("!convert ", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        PrivMsg(channel, Convertuj(message.Remove(0, 9)));
                    }
                    catch
                    {
                        PrivMsg(channel, "zle si zadal");
                    }
                }
                else if (message.StartsWith("!calc ", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        PrivMsg(channel, Calcuj(message.Remove(0, 6)));
                    }
                    catch
                    {
                        PrivMsg(channel, "zle si zadal");
                    }
                }
                else if (message.StartsWith("!urban ", StringComparison.OrdinalIgnoreCase) || message.Equals("!urban", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        PrivMsg(channel, Urbanuj(message.Equals("!urban", StringComparison.OrdinalIgnoreCase)? "":message.Remove(0, 7)));
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine(e.Message);
                        PrivMsg(channel, "zle si zadal");
                    }
                }
                else if (message.StartsWith("!translate ", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        PrivMsg(channel, Translatuj(message.Remove(0, 11)));
                    }
                    catch
                    {
                        PrivMsg(channel, "zle si zadal");
                    }
                }
                else if (message.StartsWith("!ver", StringComparison.OrdinalIgnoreCase))
                {
                    if (settings.noobs[settings.noobs.IndexOf(sender)].level < 0)
                    {
                        PrivMsg(channel, "insufficient kredencials!");
                        return;
                    }
                    PrivMsg(channel, String.Format("{0}: Version {1} by {3}, {2}", Version.AssemblyProduct, Version.AssemblyVersion, Version.AssemblyCopyright, Version.AssemblyCompany));
                }
                else if (message.StartsWith("!quit", StringComparison.OrdinalIgnoreCase))
                {
                    if (settings.noobs[settings.noobs.IndexOf(sender)].level < 10)
                    {
                        PrivMsg(channel, "insufficient kredencials!");
                        return;
                    }
                    irc.RfcQuit("Sajonara buzny!");
                    Environment.Exit(1337);
                }
                //?? v texte
                else if (message.ToLower().Contains("?? "))
                {
                    Message_QQ_text(message, channel, sender);
                }
                //spellz
                /*else if (message.StartsWith("!cast ", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Cast(message, channel, sender);
                }*/
                // ATTACK MODE
                else if (message.StartsWith("!attack ", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Attack(message, channel, sender);
                }
                else if (message.Equals("myinfo", StringComparison.OrdinalIgnoreCase))
                {
                    PrivMsg(channel, String.Format("{0} tvoje info: {1} {2}", sender, irc.GetIrcUser(sender).Realname, irc.GetIrcUser(sender).Host));
                }
                // nastavenie klasy
                /*else if (message.StartsWith("!setclass", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Setclass(message, channel, sender);
                }*/
                // halp!!
                else if (message.StartsWith("!help", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Commands(channel, settings.noobs[settings.noobs.IndexOf(sender)].level); 
                    /*PrivMsg(
                                    sender,
                                    "availiable commands: !setclass, !attack, !cast, !spells, !stats, !quote, !addquote, !addgreet, ??, info about commands ?? <command> (without exclamation mark)");*/
                }
                else if (message.Equals("!commands", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Commands(channel, settings.noobs[settings.noobs.IndexOf(sender)].level);   
                }
                // vypis statsov
                /*else if (message.StartsWith("!stats", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Stats(message, channel, sender);
                }   */           
                else if (message.StartsWith("!addgreet ", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Addgreet(message, channel);
                }
                else if (message.StartsWith("!qogstats", StringComparison.OrdinalIgnoreCase)||message.Equals("!teplosi",StringComparison.OrdinalIgnoreCase))
                {
                    Message_QOGstats(message, channel, sender);
                }
                else if (message.StartsWith("!qog", StringComparison.OrdinalIgnoreCase))
                {
                    Message_QOG(message, channel, sender);
                }
                else if (message.StartsWith("!roll", StringComparison.OrdinalIgnoreCase))
                {
                    Message_roll(message, channel, sender);
                }
                else if (message.StartsWith("!rickroll", StringComparison.OrdinalIgnoreCase))
                {
                    Message_rickroll(message, channel, sender);
                }
                else if (message.StartsWith("!learned", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Learned(channel,sender);
                }
                else if (message.Equals("!fajci", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Fajci(channel, sender);
                }
                else if (message.Equals("!opme", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Opme(channel, sender);
                }
                else if (message.Equals("!voiceme", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Voiceme(channel, sender);
                }
                else if (message.Equals("!id", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Id(channel, sender);
                }
                else if (message.StartsWith("!op ", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Op(channel, sender,message);
                }
                else if (message.StartsWith("!deop ", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Deop(channel, sender, message);
                }
                else if (message.StartsWith("!voice ", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Voice(channel, sender, message);
                }
                else if (message.StartsWith("!devoice ", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Devoice(channel, sender, message);
                }
                else if (message.Equals("!level", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Level(channel, sender, message);
                }
                else if (message.StartsWith("!setlevel ", StringComparison.OrdinalIgnoreCase))
                {
                    Message_SetLevel(channel, sender, message);
                }
                else if (message.StartsWith("!unlearn ", StringComparison.OrdinalIgnoreCase))
                {
                    Message_Unlearn(message, channel , sender);
                }
                // na salata :>
                /*else if (message.ToLower().Contains("shala")
                              || message.ToLower().Contains("salat"))
                {
                    Message_Salat(channel, sender);
                }*/
                // SMAJLY! :D
                /*else if (message.Contains(":)") || message.Contains(":]")
                                || message.Contains(":>") || message.Contains(":D")
                                || message.Contains("=)") || message.Contains("8)")
                                || message.Contains("xD") || message.Contains(":}")
                                || message.Contains("^^") || message.Contains("^_^")
                                || message.Contains(":-)") || message.Contains(":o)")
                                || message.Contains("8-)") || message.Contains(":-D")
                                || message.Contains(";)") || message.Contains(";-)")
                                || message.Contains(";D") || message.Contains("(:")
                                || message.Contains("<:") || message.Contains("haha")
                                || message.Contains("HAHA") || message.Contains("hehe")
                                || message.Contains("HEHE") || message.Contains("hihi")
                                || message.Contains("HIHI") || message.Contains("hoho")
                                || message.Contains("HOHO")
                                || (message.Contains("lo") && message.Contains("ol"))
                                || message.Contains("rofl") || message.Contains("lmao")
                                || message.Contains("xex"))
                {
                    Message_Smajle(channel);
                }*/
                /*else if (message.Contains(":(") || message.Contains(":[")
                                || message.Contains(":<") || message.Contains(":{")
                                || message.Contains("D:") || message.Contains(":-(")
                                || message.Contains(":o(") || message.Contains(":\\")
                                || message.Contains(">.<") || message.Contains(">_<")
                                || message.Contains("x.x") || message.Contains("x_x")
                                || (message.Contains(":_") && message.Contains("_(")))
                {

                    Message_Plackovia(channel, sender);
                }*/
                // DANCING, YEAH!
                else if ((message.ToLower().Contains("dance"))
                              && !settings.noobs[settings.noobs.IndexOf(sender)].danced)
                {
                    irc.SendMessage(SendType.Action, channel, "dances: :D\\-<");
                    irc.SendMessage(SendType.Action, channel, "dances: :D|-=");
                    irc.SendMessage(SendType.Action, channel, "dances: :D/-\u2248");
                    irc.SendMessage(SendType.Action, channel, "dances: :D|-=");
                    irc.SendMessage(SendType.Action, channel, "dances: :D\\-<");
                    settings.noobs[settings.noobs.IndexOf(sender)].danced = true;
                }
                /*else if (message.Contains("!!") || message.Contains("!1") || message.Contains("1!") || message.Contains("!one"))
                {
                    Message_yelling(channel, sender);
                }*/
            }
            /**
                * QOG (Quality Of Gay) Calculator
                */
            QogCalculator(sender, message);
        }

                                              

        private void DecreaseStamina(string sender, float amount)
        {
            String query = String.Format("UPDATE irc_qog SET stamina = MAX(stamina - {0}, 0) WHERE nick = '{1}'", amount, sender);
            dc.sendUpdateQuery(query);
        }

        void OnQueryMessage(object senderr, IrcEventArgs e)
        {
            string message = e.Data.Message;
            string sender = e.Data.Nick;
            if (message.StartsWith("!say ", StringComparison.OrdinalIgnoreCase))
            {
                if (settings.noobs[settings.noobs.IndexOf(sender)].level < 5)
                {
                    PrivMsg(sender, "insufficient kredencials!");
                    return;
                }
                string who;
                try
                {
                    who = message.Split(' ')[1];
                }
                catch
                {
                    return;
                }
                try
                {
                    PrivMsg(who, message.Remove(0, 6 + who.Length));
                }
                catch
                {
                    PrivMsg(sender, "nedal si kam to mam napisat ty hlupka!");
                }
            }
            else if (message.StartsWith("!pass ", StringComparison.OrdinalIgnoreCase))
            {
                if (settings.noobs[settings.noobs.IndexOf(sender)].level < 1)
                {
                    PrivMsg(sender, "insufficient kredencials!");
                    return;
                }
                string newpass;
                try
                {
                    newpass = message.Split(' ')[1];
                }
                catch
                {
                    return;
                }
                using (SHA1 sha = new SHA1CryptoServiceProvider())
                {
                    settings.userlist[sender]=String.Format("{0} {1}", settings.noobs[settings.noobs.IndexOf(sender)].level, BitConverter.ToString(sha.ComputeHash(Encoding.UTF8.GetBytes(newpass))));
                }
                settings.SaveAuthUsers();
            }
            else if (message.StartsWith("!pass ", StringComparison.OrdinalIgnoreCase))
            {
                if (settings.noobs[settings.noobs.IndexOf(sender)].level < 1)
                {
                    PrivMsg(sender, "insufficient kredencials!");
                    return;
                }
                string newpass;
                try
                {
                    newpass = message.Split(' ')[1];
                }
                catch
                {
                    return;
                }
                using (SHA1 sha = new SHA1CryptoServiceProvider())
                {
                    settings.userlist[sender] = String.Format("{0} {1}", settings.noobs[settings.noobs.IndexOf(sender)].level, BitConverter.ToString(sha.ComputeHash(Encoding.UTF8.GetBytes(newpass))));
                }
                settings.SaveAuthUsers();
                PrivMsg(sender, "password successfully changed");
            }
            else if (message.StartsWith("!logout", StringComparison.OrdinalIgnoreCase))
            {
                if (settings.noobs[settings.noobs.IndexOf(sender)].level < 1)
                {
                    PrivMsg(sender, "insufficient kredencials!");
                    return;
                }
                settings.noobs[settings.noobs.IndexOf(sender)].level = 0;
                PrivMsg(sender, "you are now logged out");
            }
            else if(message.StartsWith("!login ",StringComparison.OrdinalIgnoreCase))
            {
                if (settings.noobs[settings.noobs.IndexOf(sender)].level > 0)
                {
                    PrivMsg(sender, "uz si nalogovany!");
                    return;
                }
                string pass;
                try
                {
                    pass = message.Split(' ')[1];
                }
                catch
                {
                    return;
                }
                int level = GetAuthLevel(sender, pass);
                settings.noobs[settings.noobs.IndexOf(sender)].level = level;
                if (level > 0) PrivMsg(sender, "login successfull!");
                else PrivMsg(sender, "you are not registered or bad password");
            }
            else if (message.StartsWith("!join ", StringComparison.OrdinalIgnoreCase))
            {
                if (settings.noobs[settings.noobs.IndexOf(sender)].level < 5)
                {
                    PrivMsg(sender, "insufficient kredencials!");
                    return;
                }
                string pass;
                try
                {
                    pass = message.Split(' ')[1];
                }
                catch
                {
                    return;
                }
                Join(pass);
            }
            else if (message.StartsWith("!part ", StringComparison.OrdinalIgnoreCase))
            {
                if (settings.noobs[settings.noobs.IndexOf(sender)].level < 5)
                {
                    PrivMsg(sender, "insufficient kredencials!");
                    return;
                }
                string pass;
                try
                {
                    pass = message.Split(' ')[1];
                }
                catch
                {
                    return;
                }
                Part(pass);
            }
            else if (message.StartsWith("!register ", StringComparison.OrdinalIgnoreCase))
            {
                string pass;
                try
                {
                    pass = message.Split(' ')[1];
                }
                catch
                {
                    return;
                }
                if (!settings.userlist.ContainsKey(sender))
                {
                    using (SHA1 sha = new SHA1CryptoServiceProvider())
                    {
                        settings.userlist.Add(sender, "1 " + BitConverter.ToString(sha.ComputeHash(Encoding.UTF8.GetBytes(pass))));
                    }
                    settings.noobs[settings.noobs.IndexOf(sender)].level = 1;
                    settings.SaveAuthUsers();
                    PrivMsg(e.Data.Nick, "registration successfull!");
                }
                else
                {
                    PrivMsg(e.Data.Nick, "user already registered!");
                }
            }
            else
            {
                Console.WriteLine(e.Data.Type + ":");
                Console.WriteLine(String.Format("(private) <{0}> {1}", e.Data.Nick, e.Data.Message));
                PrivMsg(e.Data.Nick, "naserem ti do xrapi!");
            }
        }

        private int GetAuthLevel(string who, string pass)
        {
            using (SHA1 sha = new SHA1CryptoServiceProvider())
            {
                if (settings.userlist.ContainsKey(who))
                {
                    if (settings.userlist[who].Split(' ')[1].Equals(BitConverter.ToString(sha.ComputeHash(Encoding.UTF8.GetBytes(pass)))))
                        return Convert.ToInt32(settings.userlist[who].Split(' ')[0]);
                }
            }
            return 0;
        }

        void OnKick(object sender, KickEventArgs e)
        {
            irc.RfcJoin(e.Channel);
            PrivMsg(e.Channel, String.Format("omgwtf {0}!!!", e.Who));
        }

        public void Quit()
        {
            running = false;
            irc.RfcQuit("Sajonara buzny!");
            qogthread.Abort();
            
        }

        void PendQOG(String user, float value)
        {
            //if (DateTime.Now.Subtract(settings.noobs[settings.noobs.IndexOf(user)].lastqog).TotalMilliseconds > 3000)
            {
                if (generator.Next(0, 7) == 1) value = -value;
                settings.noobs[settings.noobs.IndexOf(user)].qogpend += value;
                settings.noobs[settings.noobs.IndexOf(user)].lastqog = DateTime.Now;
                Console.WriteLine(String.Format("pridavam do qog pending {0} celkovo {1}", value, settings.noobs[settings.noobs.IndexOf(user)].qogpend));
            }
            
        }

        void QogCalculator(String user, String message)
        {
            string[] sp;
            string key;
            foreach(string line in settings.qogwords)
            {
                sp = line.Split(' ');
                key = "";
                for (int i = 0; i < sp.Length - 1; i++)
                    key += String.Format("{0} ", sp[i]);
                key = key.Trim();
                CultureInfo ci=new CultureInfo("en-us");
                if (message.ToLower().Contains(key))
                {
                    //PendQOG(user, float.Parse(sp[sp.Length - 1], ci));
                    PendQOG(user, (float)(Math.Round(generator.NextDouble(),1)+0.3));
                }
            }
        }
        public void RefreshQogWords()
        {
            settings.LoadQogWords();
        }
        public void RefreshSpells()
        {
            settings.LoadSpells();
        }

        private static int GetXp(int who_level)
        {
            switch (who_level)
            {
                case 1:
                    return 100;
                case 2:
                    return 120;
                case 3:
                    return 160;
                case 4:
                    return 220;
                default:
                    return (who_level-2)*100;
            }
        }


        static string Googluj(string searchTerm)
        {
            /* ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
             string search = string.Format("https://www.google.com/search?q={0}&safe=off", searchTerm);
             System.Net.WebClient client = new System.Net.WebClient() { Encoding = Encoding.UTF8 };
             client.Headers.Add("user-agent", userAgent);
             var html = client.DownloadString(search);*/
            string search = string.Format("http://www.google.com/search?q={0}&safe=off", searchTerm);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(search);
            request.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
            request.UserAgent = userAgent;
            string html="";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response != null)
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        html = reader.ReadToEnd();
                        int index = html.IndexOf("<div class=\"rc\"><div class=\"r\"><a href=\"");
                        int konec = html.IndexOf("\" onmousedown", index);
                        html = html.Substring(index + 40, konec - index - 40);
                    }
                }
                else
                {
                    Console.WriteLine("secko je fpici");
                }
            }
            /*HttpWebRequest request = (HttpWebRequest)WebRequest.Create(search);
            request.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
            request.UserAgent = userAgent;
            string html;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response != null)
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        html = reader.ReadToEnd();
                        // MessageBox.Show(html);
                        int index = html.IndexOf("<h3 class=\"r\"><a href=\"");
                        int konec = html.IndexOf("\" onmousedown", index);
                        html=html.Substring(index + 23, konec - index - 23);
                    }
                }
                else return "skus znova";
            }*/
            return html.Replace("&amp;","&");          
        }

        static string Convertuj(string searchTerm)
        {
            string search = string.Format("http://www.google.com/search?q={0}&safe=off&nfpr=1", searchTerm);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(search);
            request.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
            request.UserAgent = userAgent;
            string html;
            string cast;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response != null)
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        html = reader.ReadToEnd();
                        // MessageBox.Show(html);
                        int index = html.IndexOf("vk_gy vk_sh")+13;
                        if (index == 12) return "zle si zadal";
                        int konec = html.IndexOf("</div>", index);
                        cast = html.Substring(index, konec - index);
                        index = html.IndexOf("dDoNo vk_bk") + 13;
                        konec = html.IndexOf("</div>", index);
                        cast = cast + " " + html.Substring(index, konec - index);
                    }
                }
                else return "skus znova";
            }
            return cast.Replace(" &#215; ","x").Replace("<sup>","^").Replace("</sup>","");
        }
        static string Calcuj(string searchTerm)
        {
            string search = string.Format("http://www.google.com/search?q={0}&safe=off", searchTerm.Replace("+", "%2B"));

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(search);
            request.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
            request.UserAgent = userAgent;
            string html;
            string cast;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response != null)
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        html = reader.ReadToEnd();
                        //MessageBox.Show(html);
                        //Console.WriteLine(html);
                        int index = html.IndexOf("cwles") + 9;
                        if (index == 8) return "zle si zadal";
                        int konec = html.IndexOf("  </span>", index);
                        cast = html.Substring(index, konec - index);
                        index = html.IndexOf("cwos") + 7;
                        konec = html.IndexOf(" </span>", index);
                        cast = cast + " " + html.Substring(index, konec - index);
                    }
                }
                else return "skus znova";
            }
            return cast.Replace(" &#215; ", "x").Replace("<sup>", "^").Replace("</sup>", "");
        }

        static string Urbanuj(string searchTerm)
        {
            string search;
            if (string.IsNullOrEmpty(searchTerm))
                search = string.Format("https://www.urbandictionary.com/random.php?page=5000");
            else
                search = string.Format("https://www.urbandictionary.com/define.php?term={0}", searchTerm.Replace("+", "%2B").Replace(" ", "%20"));

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(search);
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                    | SecurityProtocolType.Tls11
                                                    | SecurityProtocolType.Tls12
                                                    | SecurityProtocolType.Ssl3;
            request.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
            request.UserAgent = userAgent;
            string html;
            string definition, word;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response != null)
                {
                    string respurl = response.ResponseUri.ToString();
                    using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        html = reader.ReadToEnd();
                        //MessageBox.Show(html);
                        //Console.WriteLine(html);
                        int index = html.IndexOf("<a class=\"word\" href=\"");
                        if (index == -1) return "take slovo tam neni";
                        index = html.IndexOf(">", index) + 1;
                        //Console.WriteLine(index.ToString());
                        //if(index==32) return "take slovo tam neni";
                        //if (index == 8) return "zle si zadal";
                        int konec = html.IndexOf("<", index);
                        word =  html.Substring(index, konec - index).Replace("&#39;", "'").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&"); ;
                        //Console.WriteLine(word);
                        index = html.IndexOf("<div class='meaning'>");
                        if(index==-1)
                            index = html.IndexOf("<div class=\"meaning\">");
                        if (index == -1)
                            return "pojebabralo sa to!";
                        index = html.IndexOf(">", index) + 1;
                        konec = html.IndexOf("</div>", index);
                        definition = html.Substring(index, konec - index);
                        while (definition.Contains("<"))
                        {
                            definition = definition.Remove(definition.IndexOf("<"), definition.IndexOf(">", definition.IndexOf("<")) - definition.IndexOf("<"));
                           StringBuilder strb = new StringBuilder(definition);
                            strb[definition.IndexOf(">")] = ' ';
                            definition = strb.ToString();
                        }
                        definition=definition.Replace(System.Environment.NewLine, " ").Replace('\n', ' ').Replace("\r", ", ").Replace("&#39;", "'").Replace("&quot;", "\"").Replace("  "," ").Replace("  ", " ").Replace("  ", " ").Replace("  ", " ").Replace("&apos;","'").Replace("&amp;","&");


                        definition = word + " = " + definition;
                        if (definition.Length>800)
                        {
                            definition = definition.Substring(0, 800) + " " + respurl.Replace("+", "%2B").Replace(" ", "%20");
                        }
                        return definition;
                    }
                }
                else return "skus znova";
            }
        }

        static string Translatuj(string searchTerm)
        {
            string[] words = searchTerm.Split(' ');
            string query = "translate " + searchTerm.Replace(words[words.Length - 2], "from " + words[words.Length - 2] + " to ");
            string search = string.Format("http://www.google.com/search?q={0}&safe=off", query.Replace("+", "%2B"));

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(search);
            request.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
            request.UserAgent = userAgent;
            string html;
            string cast;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response != null)
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        html = reader.ReadToEnd();
                        //MessageBox.Show(html);
                        //Console.WriteLine(html);
                        int index = html.IndexOf("id=\"tw-source-text\"") + 50;
                        if (index == 49) return "zle si zadal";
                        int konec = html.IndexOf("</span>", index);
                        cast = html.Substring(index, konec - index);
                        index = html.IndexOf("id=\"tw-target-text\"") + 50;
                        konec = html.IndexOf("</span>", index);
                        cast = cast + " = " + html.Substring(index, konec - index);
                    }
                }
                else return "skus znova";
            }
            return cast.Replace(" &#215; ", "x").Replace("<sup>", "^").Replace("</sup>", "");
        }

        static string Binguj(string searchTerm)
        {
            string search = string.Format("http://www.bing.com/search?q={0}", searchTerm);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(search);
            string html;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response != null)
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.ASCII))
                    {
                        html = reader.ReadToEnd();
                        reader.Close();
                    }
                    response.Close();
                }
                else return "skus znova";
            }
            int index = html.IndexOf("<li class=\"b_algo\"><h2><a href=\"");
            int konec = html.IndexOf("\" h=\"ID=SERP,", index);
            Console.WriteLine(index);
            Console.WriteLine(konec);
            html = html.Substring(index + 32, konec - index - 32);
            //Console.Write(html);
            return html;// DajPrvyHit(html, "(<div class=\"sb_tlst\"><h3><a href=\")");
        }

        static string DajPrvyHit(string html,string regexp)
        {
            Match match = Regex.Match(html, String.Format("{0}(([a-zA-Z][0-9a-zA-Z+\\-\\.]*:)?/{{0,2}}[0-9a-zA-Z;/?:@&=+$\\.\\-_!~*'()%]+)?(#[0-9a-zA-Z;/?:@&=+$\\.\\-_!~*'()%]+)?", regexp));
            try
            {
                string vysledok = match.Groups[2].Value;
                if (string.IsNullOrEmpty(vysledok)) throw new Exception("dojeeeebaliii koneee woooooz!!");
                return vysledok;
            }
            catch
            {
                return "nic neni!";
            }
        }

        internal void Disconnect()
        {
            irc.Disconnect();
        }

        internal void Join(string chan)
        {
            if(chan=="")irc.RfcJoin(settings.chan);
            else irc.RfcJoin(chan);
        }
        internal void Part(string chan)
        {
            if (chan == "") irc.RfcPart(settings.chan);
            else irc.RfcPart(chan);
        }

        internal void ChangeNick(string chan)
        {
            irc.RfcNick(chan);
        }
    }
}
