﻿using System;
using System.Globalization;
using System.Threading;
using System.Data.SQLite;

namespace shalatik
{
    partial class Shalatik
    {
        void UpdateQOG(String user, float value, String channel)
        {
            String query = String.Format("SELECT qog, level, HP, CLASS FROM irc_qog WHERE nick = '{0}'", user);
            using (SQLiteDataReader rs = dc.sendQuery(query))
            {
                if (rs.Read())
                {
                    if (Convert.ToSingle(rs["qog"]) + value > 100.0f)
                    {
                        query = String.Format("UPDATE irc_qog SET qog = {0}, level = level + 1 WHERE nick = '{1}'", (Convert.ToSingle(rs["qog"]) + value - 100.0f), user);
                        dc.sendUpdateQuery(query);
                        irc.RfcPrivmsg(channel, String.Format("woot! {0}{1}{0} is now level {2}", Colors.BOLD, user, (Convert.ToInt32(rs["level"]) + 1)));
                        if (Convert.ToInt32(rs["level"]) + 1 == 25)
                        {
                            query = "UPDATE irc_qog SET class = ''";
                            dc.sendUpdateQuery(query);
                            query = "UPDATE irc_qog SET xp = 0";
                            dc.sendUpdateQuery(query);
                            query = "UPDATE irc_qog SET mana = 0";
                            dc.sendUpdateQuery(query);
                            query = "UPDATE irc_qog SET hp = 0";
                            dc.sendUpdateQuery(query);
                            query = "UPDATE irc_qog SET level = 1";
                            dc.sendUpdateQuery(query);
                            irc.RfcPrivmsg(settings.chan[0], String.Format("{0}Wooot!! {0}{1}{2} has won!!", Colors.BOLD, Colors.GREEN, user));
                            foreach (Noob nab in settings.noobs)
                            {
                                nab.danced = false;
                            }
                        }
                    }
                    else
                    {
                        query = String.Format("UPDATE irc_qog SET qog = qog + {0} WHERE nick = '{1}'", value, user);
                        dc.sendUpdateQuery(query);
                    }
                }
                else
                {
                    query = String.Format("INSERT INTO irc_qog VALUES (null, '{0}', {1}, 1,0,0,'',0,0)", user, value);
                    dc.sendUpdateQuery(query);
                }
                rs.Close();
            }
        }

        void QogThread(object obj)
        {
            CultureInfo ci = new CultureInfo("en-us");
            Thread.CurrentThread.CurrentCulture = ci;
            DatabaseConnect dc=(DatabaseConnect)obj;
            while (running)
            {
                Thread.Sleep(900000);
                if (lastdanced.Subtract(DateTime.Now).TotalSeconds < 0)
                {
                    for(int i=0;i<settings.noobs.Count;i++)
                    {
                        settings.noobs[i].danced = false;
                        settings.noobs[i].sucked = false;
                    }
                    lastdanced = DateTime.Now.AddHours(generator.Next(24) + 24).AddMinutes(generator.Next(61));
                    irc.RfcPrivmsg(settings.chan[0], "Lambada!");
                }
                lock (dc)
                {
                    Console.WriteLine("ubdatujem qog!");
                    for (int i = 0; i < settings.noobs.Count; i++)
                    {
                        UpdateQOG(settings.noobs[i].name, settings.noobs[i].qogpend, settings.chan[0]);
                        settings.noobs[i].qogpend = 0;
                    }
                }
            }
        }
        public void ConnectThread()
        {
            CultureInfo ci = new CultureInfo("en-us");
            Thread.CurrentThread.CurrentCulture = ci;
            try
            {
                try { irc.Disconnect(); }
                catch { }
                Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public void WatchDog(object trd)
        {
            Thread connect = (Thread)trd;
            CultureInfo ci = new CultureInfo("en-us");
            Thread.CurrentThread.CurrentCulture = ci;
            while (running)
            {
                Thread.Sleep(60000);
                foreach (string str in settings.chan)
                {
                    if (!irc.IsJoined(str)) irc.RfcJoin(str);
                }
                if (irc.Nickname != settings.nick) irc.RfcNick(settings.nick);
                if (!GetStav())
                {
                    try
                    {
                        connect.Abort();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    connect = new Thread(ConnectThread);
                    connect.Start();
                }
            }
        }
        private static int GetLvlupXP(int level)
        {
            int expy=0;
            for (int i = 1; i <= level; i++)
            {
                expy += (i + 1) * 100;
            }
            return expy;
        }

        void CooldownThread()
        {
            int waittime = 2000;
            while (running)
            {
                Thread.Sleep(waittime);
                foreach (Noob nab in settings.noobs)
                {
                    nab.cooldown = Math.Max(0, nab.cooldown - waittime);
                    nab.attackcd = Math.Max(0, nab.attackcd - waittime);
                }
            }
        }

        void RegenThread(object obj)
        {
            String query = "";
            CultureInfo ci = new CultureInfo("en-us");
            Thread.CurrentThread.CurrentCulture = ci;
            DatabaseConnect dc = (DatabaseConnect)obj;
            while (running)
            {
                Thread.Sleep(settings.regeninterval);
                lock (dc)
                {
                    query = "SELECT * FROM irc_qog";
                    using (SQLiteDataReader rs = dc.sendQuery(query))
                    {
                        while (rs.Read())
                        {
                            int level = Convert.ToInt32(rs["level"]);
                            String user = rs["nick"].ToString();
                            String klass = rs["class"].ToString();
                            int hp = Convert.ToInt32(rs["hp"]);
                            int mana = Convert.ToInt32(rs["mana"]);
                            int xp = Convert.ToInt32(rs["xp"]);
                            if (xp >= GetLvlupXP(level))
                            {
                                if (level == 24)
                                {
                                    query = "UPDATE irc_qog SET class = ''";
                                    dc.sendUpdateQuery(query);
                                    query = "UPDATE irc_qog SET xp = 0";
                                    dc.sendUpdateQuery(query);
                                    query = "UPDATE irc_qog SET mana = 0";
                                    dc.sendUpdateQuery(query);
                                    query = "UPDATE irc_qog SET hp = 0";
                                    dc.sendUpdateQuery(query);
                                    query = "UPDATE irc_qog SET level = 1";
                                    dc.sendUpdateQuery(query);
                                    irc.RfcPrivmsg(settings.chan[0], String.Format("{0}Wooot!! {0}{1}{2} has won!!", Colors.BOLD, Colors.GREEN, user));
                                    foreach (Noob nab in settings.noobs)
                                    {
                                        nab.danced = false;
                                    }
                                }
                                else
                                {
                                    query = String.Format("UPDATE irc_qog SET level = level + 1 WHERE nick = '{0}'", user);
                                    dc.sendUpdateQuery(query);
                                    irc.RfcPrivmsg(settings.chan[0], String.Format("{0}Woot!!{0} {1}{2}{3} has advanced to level {4}!!one", Colors.BOLD, Colors.RED, user, Colors.NORMAL, level + 1));
                                }
                            }
                            float stamina = Convert.ToSingle(rs["stamina"]);
                            float maxHP = 0;
                            float maxMana = 0;
                            float maxStamina = 0;
                            float addHP = 0;
                            float addMana = 0;
                            float addStamina = 0;
                            if (klass.Equals("agi"))
                            {
                                maxHP = settings.agihpkoef * settings.hpkoef * level + settings.agibasehp;
                                maxMana = settings.agimanakoef * settings.manakoef * level + settings.agibasemana;
                                maxStamina = settings.agistaminakoef * settings.staminakoef * level + settings.agibasestamina;
                                addHP = settings.agihpkoef * level / 5;
                                addMana = settings.agimanakoef * level / 5;
                                addStamina = settings.agistaminakoef * level;
                            }
                            if (klass.Equals("int"))
                            {
                                maxHP = settings.inthpkoef * settings.hpkoef * level + settings.intbasehp;
                                maxMana = settings.intmanakoef * settings.manakoef * level + settings.intbasemana;
                                maxStamina = settings.intstaminakoef * settings.staminakoef * level + settings.intbasestamina;
                                addHP = settings.inthpkoef * level / 5;
                                addMana = settings.intmanakoef * level / 5;
                                addStamina = settings.intstaminakoef * level;
                            }
                            if (klass.Equals("str"))
                            {
                                maxHP = settings.strhpkoef * settings.hpkoef * level + settings.strbasehp;
                                maxMana = settings.strmanakoef * settings.manakoef * level + settings.strbasemana;
                                maxStamina = settings.strstaminakoef * settings.staminakoef * level + settings.strbasestamina;
                                addHP = settings.strhpkoef * level / 5;
                                addMana = settings.strmanakoef * level / 5;
                                addStamina = settings.strstaminakoef * level;
                            }
                            if (klass.Equals("str") || klass.Equals("int") || klass.Equals("agi"))
                            {
                                if (hp < maxHP && hp > 0)
                                {
                                    query = String.Format("UPDATE irc_qog SET hp = {0} WHERE nick = '{1}'", Math.Min(maxHP, hp + addHP), user);
                                    dc.sendUpdateQuery(query);
                                }
                                if (mana < maxMana)
                                {
                                    query = String.Format("UPDATE irc_qog SET mana = {0} WHERE nick = '{1}'", Math.Min(maxMana, addMana + mana), user);
                                    dc.sendUpdateQuery(query);
                                }
                                if (stamina < maxStamina)
                                {
                                    query = String.Format("UPDATE irc_qog SET stamina = {0} WHERE nick = '{1}'", Math.Min(maxStamina, stamina + addStamina), user);
                                    dc.sendUpdateQuery(query);
                                }
                            }
                        }
                        rs.Close();
                    }
                }
            }
        }
        void RespawnThread(object obj)
        {
            String query = "";
            CultureInfo ci = new CultureInfo("en-us");
            Thread.CurrentThread.CurrentCulture = ci;
            DatabaseConnect dc = (DatabaseConnect)obj;
            while (running)
            {
                Thread.Sleep(settings.respawninterval);
                GC.Collect();
                lock (dc)
                {
                    query = "SELECT * FROM irc_qog";
                    using (SQLiteDataReader rs = dc.sendQuery(query))
                    {
                        while (rs.Read())
                        {
                            int level = Convert.ToInt32(rs["level"]);
                            String user = rs["nick"].ToString();
                            if (Convert.ToInt32(rs["hp"]) <= 0)
                            {
                                float hp = 0, mana = 0, stamina = 0;
                                // nastavovanie statsov ak su nulove, len ak je aj klasa
                                string klasa = rs["CLASS"].ToString();
                                if (klasa.Equals("agi"))
                                {
                                    hp = settings.agihpkoef * settings.hpkoef * level + settings.agibasehp;
                                    mana = settings.agimanakoef * settings.manakoef * level + settings.agibasemana;
                                    stamina = settings.agistaminakoef * settings.staminakoef * level + settings.agibasestamina;
                                }
                                if (klasa.Equals("int"))
                                {
                                    hp = settings.inthpkoef * settings.hpkoef * level + settings.intbasehp;
                                    mana = settings.intmanakoef * settings.manakoef * level + settings.intbasemana;
                                    stamina = settings.intstaminakoef * settings.staminakoef * level + settings.intbasestamina;
                                }
                                if (klasa.Equals("str"))
                                {
                                    hp = settings.strhpkoef * settings.hpkoef * level + settings.strbasehp;
                                    mana = settings.strmanakoef * settings.manakoef * level + settings.strbasemana;
                                    stamina = settings.strstaminakoef * settings.staminakoef * level + settings.strbasestamina;
                                }
                                if (klasa.Equals("str") || klasa.Equals("agi") || klasa.Equals("int"))
                                {
                                    query = String.Format("UPDATE irc_qog SET HP = {0}, mana = {1}, stamina = {2} WHERE nick = '{3}'", hp, mana, stamina, user);
                                    dc.sendUpdateQuery(query);
                                    irc.RfcPrivmsg(settings.chan[0], String.Format("{0}{1}{2} has respawned!", Colors.GREEN, rs["nick"], Colors.NORMAL));
                                }
                            }
                        }
                        rs.Close();
                    }
                }
            }
        }
    }
}