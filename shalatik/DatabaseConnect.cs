﻿using System;
using System.Data.SQLite;
using System.Data;

namespace shalatik
{
    class DatabaseConnect
    {
        SQLiteConnection con;
        SQLiteConnection con2;

        public DatabaseConnect(SQLiteConnection con, SQLiteConnection con2)
        {
            this.con = con;
            this.con2 = con2;
        }

        public SQLiteDataReader sendQuery(String query)
        {
            try
            {
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    return stmt.ExecuteReader();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(String.Format("DOOOOOPIIIIIIICEEEEEE!!! dbcreader\n{0}\n{1}",e.StackTrace,e.Message));
                con.Close();
                con = new SQLiteConnection(con.ConnectionString);
                con.Open();
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    return stmt.ExecuteReader();
                }
            }         
        }
        public SQLiteDataReader sendQuery(String query, String param)
        {
            try
            {
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    stmt.Parameters.Add("@name", DbType.String).Value = param;
                    return stmt.ExecuteReader();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("DOOOOOPIIIIIIICEEEEEE!!! dbcreader\n{0}\n{1}", e.StackTrace, e.Message));
                con.Close();
                con = new SQLiteConnection(con.ConnectionString);
                con.Open();
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    return stmt.ExecuteReader();
                }
            }
        }
        public SQLiteDataReader sendQuery2(String query, String param)
        {
            try
            {
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    stmt.Parameters.AddWithValue("@kvoota", String.Format("%{0}%", param));
                    return stmt.ExecuteReader();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("DOOOOOPIIIIIIICEEEEEE!!! dbcreader\n{0}\n{1}", e.StackTrace, e.Message));
                con.Close();
                con = new SQLiteConnection(con.ConnectionString);
                con.Open();
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    return stmt.ExecuteReader();
                }
            }
        }
        public SQLiteDataReader sendQuery(String query, int param)
        {
            try
            {
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    stmt.Parameters.Add("@name", DbType.Int32).Value = param;
                    return stmt.ExecuteReader();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("DOOOOOPIIIIIIICEEEEEE!!! dbcreader\n{0}\n{1}", e.StackTrace, e.Message));
                con.Close();
                con = new SQLiteConnection(con.ConnectionString);
                con.Open();
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    return stmt.ExecuteReader();
                }
            }
        }

        public void sendUpdateQuery(String query, String param)
        {
            try
            {
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    stmt.Parameters.Add("@name", DbType.String).Value = param;
                    stmt.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("DOOOOOPIIIIIIICEEEEEE!!! dbcupdate\n{0}\n{1}", e.StackTrace, e.Message));
                con.Close();
                con = new SQLiteConnection(con.ConnectionString);
                con.Open();
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    stmt.ExecuteNonQuery();
                }
            }
        }
        public void sendUpdateQuery(String query, String param, string param2)
        {
            try
            {
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    stmt.Parameters.Add("@name", DbType.String).Value = param;
                    stmt.Parameters.Add("@name2", DbType.String).Value = param2;
                    stmt.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("DOOOOOPIIIIIIICEEEEEE!!! dbcupdate\n{0}\n{1}", e.StackTrace, e.Message));
                con.Close();
                con = new SQLiteConnection(con.ConnectionString);
                con.Open();
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    stmt.ExecuteNonQuery();
                }
            }
        }
        public void sendUpdateQuery(String query, int param0, String param, String param2)
        {
            try
            {
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    stmt.Parameters.Add("@name2", DbType.String).Value = param;
                    stmt.Parameters.Add("@name3", DbType.String).Value = param2;
                    stmt.Parameters.Add("@name1", DbType.Int32).Value = param0;
                    stmt.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("DOOOOOPIIIIIIICEEEEEE!!! dbcupdate\n{0}\n{1}", e.StackTrace, e.Message));
                con.Close();
                con = new SQLiteConnection(con.ConnectionString);
                con.Open();
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    stmt.ExecuteNonQuery();
                }
            }
        }
        public void sendUpdateQuery(String query)
        {
            try
            {
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    stmt.ExecuteNonQuery();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(String.Format("DOOOOOPIIIIIIICEEEEEE!!! dbcupdate\n{0}\n{1}", e.StackTrace, e.Message));
                con.Close();
                con = new SQLiteConnection(con.ConnectionString);
                con.Open();
                using (SQLiteCommand stmt = con.CreateCommand())
                {
                    stmt.CommandText = query;
                    stmt.ExecuteNonQuery();
                }
            }       
        }

    }
}
