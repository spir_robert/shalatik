﻿using System;
using System.Xml;

namespace shalatik
{
    public class Spell
    {
        string name;
        int cost;
        int hpdmg;
        int manadmg;
        int staminadmg;
        float hpmulti;
        float manamulti;
        float staminamulti;
        bool onself;
        int cooldown;
        public Spell()
        {
            cost = 0;
            name = "bluglafnunf";
            hpdmg = 0;
            manadmg = 0;
            staminadmg = 0;
            hpmulti = 0;
            manamulti = 0;
            staminamulti = 0;
            onself = true;
            cooldown = 0;
        }
        public Spell(XmlNode node)
        {
            cost = int.Parse(node["cost"].InnerText);
            name = node["name"].InnerText;
            hpdmg = int.Parse(node["hpdmg"].InnerText);
            manadmg = int.Parse(node["manadmg"].InnerText);
            staminadmg = int.Parse(node["staminadmg"].InnerText);
            hpmulti = float.Parse(node["hpmulti"].InnerText);
            manamulti = float.Parse(node["manamulti"].InnerText);
            staminamulti = float.Parse(node["staminamulti"].InnerText);
            onself = bool.Parse(node["onself"].InnerText);
            cooldown = Int32.Parse(node["cooldown"].InnerText);
        }
        public void Damaz(int level, out int hpdamage, out int manadamage, out int staminadamage, out int cost)
        {
            if (hpdmg < 0) hpdamage = (int)Math.Min(hpdmg, level * hpmulti);
            else hpdamage = (int)Math.Max(hpdmg, level * hpmulti);
            if (manadmg < 0) manadamage = (int)Math.Min(manadmg, level * manamulti);
            else manadamage = (int)Math.Max(manadmg, level * manamulti);
            if (staminadmg < 0) staminadamage = (int)Math.Min(staminadmg, level * staminamulti);
            else staminadamage = (int)Math.Max(staminadmg, level * staminamulti);
            cost = this.cost;
        }
        public string Name { get { return name; } }
        public int Cooldown { get { return cooldown; } }
        public bool Onself { get { return onself; } }
    }
}
