﻿using System;
using System.Reflection;

namespace shalatik
{
    static class Version
    {
        public static string AssemblyVersion
        {
            get
            {
                String tmp;
                tmp = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                DateTime strt = new DateTime(2000, 1, 1);
                strt = strt.AddDays((double)Assembly.GetExecutingAssembly().GetName().Version.Build);
                int hod, min, sek;
                sek = (Assembly.GetExecutingAssembly().GetName().Version.Revision % 30) * 2;
                min = (Assembly.GetExecutingAssembly().GetName().Version.Revision / 30) % 60;
                hod = ((Assembly.GetExecutingAssembly().GetName().Version.Revision / 30) / 60) % 24;
                String pmc = sek.ToString();
                if (pmc.Length == 1) pmc = String.Format("0{0}", pmc);
                String pmc2 = min.ToString();
                if (pmc2.Length == 1) pmc2 = String.Format("0{0}", pmc2);
                String tajm = String.Format("{0}:{1}:{2}", hod, pmc2, pmc);
                tmp += String.Format(" (compiled {0} {1}.{2}.{3})", tajm, strt.Day, strt.Month, strt.Year);
                return tmp;
            }
        }

        public static string AssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public static string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public static string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public static string AssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
    }
}
