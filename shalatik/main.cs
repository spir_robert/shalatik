﻿using System;
using System.Threading;
using System.Runtime.InteropServices;

namespace shalatik
{
    class main
    {
        [DllImport("kernel32.dll")]
        static extern bool FreeConsole();
        static Shalatik salat;
        static Thread connect, watchdog;
        static void Main()
        {
            //FreeConsole();
            System.IO.Directory.SetCurrentDirectory(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
            Thread control = new Thread(consolewait);
            control.Start();
            Settings set = new Settings("config.ini");
            salat = new Shalatik(set);
            connect = new Thread(salat.ConnectThread);
            connect.Start();
            watchdog = new Thread(salat.WatchDog);
            watchdog.Start(connect);
            control.Join();
            connect.Join();
            watchdog.Abort();
        }

        static void consolewait()
        {
            
            while (true)
            {
                string read = Console.ReadLine();
                string chan="";
                if (read.StartsWith("join") || read.StartsWith("part") || read.StartsWith("nick"))
                {
                    if (read.Split(' ').Length > 1)
                    {
                        chan = read.Split(' ')[1];
                        read = read.Split(' ')[0];
                    }
                }
                switch (read)
                {
                    case "exit":
                    case "quit":
                        salat.Quit();
                        Thread.Sleep(100);
                        watchdog.Abort();
                        connect.Abort();                      
                        return;
                    case "qog":
                        salat.RefreshQogWords();
                        break;
                    case "spells":
                        salat.RefreshSpells();
                        break;
                    case "join":
                        salat.Join(chan);
                        break;
                    case "part":
                        salat.Part(chan);
                        break;
                    case "nick":
                        salat.ChangeNick(chan);
                        break;
                    case "fefo":
                        salat.fefo = !salat.fefo;
                        break;
                    case "stav":
                        Console.WriteLine(salat.GetStav());
                        break;
                    case "disconnect":
                        try
                        {
                            salat.Disconnect();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case "connect":
                        try
                        {
                            salat.Disconnect();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        try
                        {
                            watchdog.Abort();
                            connect.Abort();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }

                        connect = new Thread(salat.ConnectThread);
                        connect.Start();
                        watchdog = new Thread(salat.WatchDog);
                        watchdog.Start(connect);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
