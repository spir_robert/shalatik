﻿using System;
using IrcClass;
using System.Collections;
using System.Data.SQLite;
using System.Text;
using System.Collections.Generic;

namespace shalatik
{
    partial class Shalatik
    {
        private void Message_AddQuote(string message, string channel)
        {
            String[] cmds = message.Split(' ');
            if (cmds.Length > 2)
            {
                String quota = "";
                String usr = cmds[cmds.Length - 1];
                if (usr.Length < 1)
                {
                    usr = cmds[cmds.Length - 2];
                    for (int i = 1; i < cmds.Length - 2; i++)
                    {
                        quota += String.Format("{0} ", cmds[i]);
                    }
                }
                else
                {
                    for (int i = 1; i < cmds.Length - 1; i++)
                    {
                        quota += String.Format("{0} ", cmds[i]);
                    }
                }
                quota=quota.Trim();
                String query = "INSERT INTO irc_quotes VALUES (null, @name, @name2)";
                dc.sendUpdateQuery(query, quota, usr);
                PrivMsg(channel, "ok citat je tam :>");
            }
        }
        private void Message_Attack(string message, string channel, string sender)
        {
            String[] cmds = message.Split(' ');

            if (cmds.Length > 2)
            {
                Random generator = new Random();
                String mes = "";

                String cim = "";

                for (int i = 3; i < cmds.Length; i++)
                    cim += cmds[i] + " ";
                cim = cim.Trim();
                // CRITICAL!
                int watafak = generator.Next(3);
                if (watafak == 2)
                {
                    int dmg = generator.Next(1500) + 1000;
                    mes = cmds[1] + " is tremendously damaged by " + sender + " with " + cim + " for a whopping " + dmg + " damage!!! Je po nem!";
                }
                else if (watafak == 1)
                {
                    int dmg = generator.Next(979) + 20;
                    mes = sender + " hits " + cmds[1] + " with " + cim + " for " + dmg + " damage!";
                }
                // MISS!
                else if (watafak == 0)
                {
                    int dmg = generator.Next(979) + 20;
                    mes = sender + " critically misses " + cmds[1] + " and hits himself for " + dmg + " damage!!";
                }

                PrivMsg(channel, mes);
            }

            /*try
            {
                String[] cmds = message.Split(' ');
                if (settings.noobs[settings.noobs.IndexOf(sender)].attackcd > 0)
                {
                    PrivMsg(channel, string.Format("{0}s cooldown remaining!", settings.noobs[settings.noobs.IndexOf(sender)].attackcd / 1000));
                    return;
                }
                if (cmds.Length > 3 && cmds[2].Equals("with", StringComparison.OrdinalIgnoreCase))
                {
                    String who = cmds[1];
                    String with = "";
                    if (sender.Equals(who, StringComparison.OrdinalIgnoreCase))
                    {
                        PrivMsg(channel, "you cannot attack yourself!");
                        return;
                    }
                    bool jenachane = false;
                    foreach (ChannelUser u in irc.GetChannel(channel).Users.Values)
                    {
                        if (u.Nick.Equals(who, StringComparison.OrdinalIgnoreCase))
                            jenachane = true;
                    }
                    if (!jenachane)
                    {
                        return;
                    }
                    for (int i = 3; i < cmds.Length; i++)
                    {
                        with += cmds[i];
                        if (i < cmds.Length - 1)
                        {
                            with += ' ';
                        }
                    }
                    String query = String.Format("SELECT id, level, hp, mana, class, stamina, xp FROM irc_qog WHERE nick = '{0}'", sender);
                    SQLiteDataReader rs = dc.sendQuery(query);
                    int sender_id = 0;
                    int sender_level = 0;
                    int sender_hp = 0;
                    int sender_mana = 0;
                    float sender_stamina = 0.0f;
                    int sender_xp = 0;
                    String sender_class = "";
                    while (rs.Read())
                    {
                        sender_id = rs.GetInt32(0);
                        sender_level = rs.GetInt32(1);
                        sender_hp = rs.GetInt32(2);
                        sender_mana = Convert.ToInt32(rs["mana"]);
                        sender_class = rs["class"].ToString();
                        sender_stamina = rs.GetFloat(5);
                        sender_xp = rs.GetInt32(6);
                    }
                    rs.Close();
                    if (sender_hp == 0) return;
                    if (sender_stamina < settings.stamina_attack)
                    {
                        PrivMsg(channel, String.Format("you are low on stamina, {0} is required!", settings.stamina_attack));
                    }
                    else
                    {
                        if (sender_id != 0)
                        {
                            query = String.Format("SELECT id, level, hp, mana, class, stamina, xp FROM irc_qog WHERE nick ='{0}'", who);
                            rs = dc.sendQuery(query);
                            int who_id = 0;
                            int who_level = 0;
                            int who_hp = 0;
                            int who_mana = 0;
                            float who_stamina = 0.0f;
                            int who_xp = 0;
                            String who_class = "";
                            while (rs.Read())
                            {
                                who_id = rs.GetInt32(0);
                                who_level = rs.GetInt32(1);
                                who_hp = rs.GetInt32(2);
                                who_mana = rs.GetInt32(3);
                                who_class = rs["class"].ToString();
                                who_stamina = rs.GetFloat(5);
                                who_xp = rs.GetInt32(6);
                            }
                            rs.Close();
                            if (who_id != 0)
                            {
                                if (who_hp <= 0)
                                {
                                    PrivMsg(channel, String.Format("{0} is already dead !", who));
                                }
                                else
                                {
                                    int dmgType = generator.Next(100);
                                    double dmgCoef = 0;
                                    int base_dmg = 0;
                                    int attackcd = 0;
                                    if (sender_class.Equals("str"))
                                    {
                                        dmgCoef = settings.strhpkoef;
                                        base_dmg = (int)(sender_level * settings.strhpkoef + settings.strbasehp / 5);
                                        attackcd = settings.attackcd;
                                    }
                                    else if (sender_class.Equals("agi"))
                                    {
                                        dmgCoef = settings.agihpkoef;
                                        base_dmg = (int)(sender_level * settings.agihpkoef + settings.agibasehp / 5);
                                        attackcd = (int)(settings.attackcd * 0.8);
                                    }
                                    else if (sender_class.Equals("int"))
                                    {
                                        dmgCoef = settings.inthpkoef;
                                        base_dmg = (int)(sender_level * settings.inthpkoef + settings.intbasehp / 5);
                                        attackcd = settings.attackcd;
                                    }
                                    settings.noobs[settings.noobs.IndexOf(sender)].attackcd = attackcd;
                                    // 80% chance to attack
                                    if (dmgType >= 0 && dmgType <= 79)
                                    {
                                        double dmgMax = sender_level * dmgCoef * 2;
                                        int dmgMaxRound = (int)Math.Round(dmgMax);
                                        int dmg = base_dmg + generator.Next(dmgMaxRound) + 1;
                                        int who_new_hp;

                                        if ((who_hp - dmg) <= 0)
                                        {
                                            who_new_hp = 0;
                                        }
                                        else
                                        {
                                            who_new_hp = who_hp - dmg;
                                        }
                                        query = String.Format("UPDATE irc_qog SET hp = {0} WHERE id ={1}", who_new_hp, who_id);
                                        dc.sendUpdateQuery(query);
                                        PrivMsg(channel, String.Format("{0} hits {1}{2}{3} with {4} for {5} damage.", sender, Colors.GREEN, who, Colors.NORMAL, with, dmg));
                                        if (who_new_hp == 0)
                                        {
                                            who_hp = 0;
                                            int xpgain = GetXp(who_level);
                                            PrivMsg(channel, String.Format("{0} {1}KILLED {2}{3} and gained {4} xp!! (total {5})", sender, Colors.RED, Colors.NORMAL, who, xpgain, (xpgain + sender_xp)));
                                            query = String.Format("UPDATE irc_qog SET xp = xp + {0} WHERE id ={1}", xpgain, sender_id);
                                            dc.sendUpdateQuery(query);
                                        }
                                        DecreaseStamina(sender, settings.stamina_attack);
                                    }
                                    // 5% change to critical
                                    else if (dmgType >= 80 && dmgType <= 84)
                                    {
                                        double dmgMax = sender_level * dmgCoef * 2;
                                        int dmgMaxRound = (int)Math.Round(dmgMax);
                                        int dmg = (base_dmg
                                                        + generator.Next(dmgMaxRound) + 1) * 2;
                                        int who_new_hp;
                                        if ((who_hp - dmg) <= 0)
                                        {
                                            who_new_hp = 0;
                                        }
                                        else
                                        {
                                            who_new_hp = who_hp - dmg;
                                        }
                                        query = String.Format("UPDATE irc_qog SET hp = {0} WHERE id ={1}", who_new_hp, who_id);
                                        dc.sendUpdateQuery(query);
                                        PrivMsg(channel, String.Format("{0} critically hits {1}{2}{3} with {4} for {5} damage !", sender, Colors.GREEN, who, Colors.NORMAL, with, dmg));
                                        if (who_new_hp == 0)
                                        {
                                            who_hp = 0;
                                            int xpgain = GetXp(who_level);
                                            PrivMsg(channel, String.Format("{0} {1}KILLED {2}{3} and gained {4} xp!! (total {5})", sender, Colors.RED, Colors.NORMAL, who, xpgain, (xpgain + sender_xp)));
                                            query = String.Format("UPDATE irc_qog SET xp = xp + {0} WHERE id ={1}", xpgain, sender_id);
                                            dc.sendUpdateQuery(query);

                                        }
                                        DecreaseStamina(sender, settings.stamina_attack);
                                    }
                                    // 10% miss
                                    else if (dmgType >= 85 && dmgType <= 94)
                                    {
                                        PrivMsg(channel, String.Format("{0} missed !", sender));
                                        DecreaseStamina(sender, settings.stamina_attack);
                                    }
                                    // 5% critical miss
                                    else if (dmgType >= 95 && dmgType <= 99)
                                    {
                                        double dmgMax = sender_level * dmgCoef
                                                        * 0.5f;
                                        int dmgMaxRound = (int)Math.Round(dmgMax);
                                        int dmg = base_dmg
                                                        + generator.Next(dmgMaxRound)
                                                        + 1;
                                        int sender_new_hp;

                                        if ((sender_hp - dmg) <= 0)
                                        {
                                            sender_new_hp = 0;
                                        }
                                        else
                                        {
                                            sender_new_hp = sender_hp - dmg;
                                        }
                                        query = String.Format("UPDATE irc_qog SET hp = {0} WHERE id ={1}", sender_new_hp, sender_id);
                                        dc.sendUpdateQuery(query);
                                        PrivMsg(channel,
                                                        String.Format("{0} critically missed and hits himself for {1} damage !", sender, dmg));
                                        if (sender_new_hp == 0)
                                        {
                                            PrivMsg(channel,
                                                            String.Format("{0}{1} KILLED HIMSELF .. HE STUPIDO !!{2}", Colors.RED, sender, Colors.NORMAL));
                                        }
                                        DecreaseStamina(sender, settings.stamina_attack);
                                    }
                                }
                                //////////////////////////////////////////////////////
                                if (who_hp == 0) return;
                                if (who_stamina < settings.stamina_attack)
                                {
                                    PrivMsg(channel, String.Format("{0} doesn't have enough stamina to strike back!", who));
                                    return;
                                }
                                if (sender_hp <= 0)
                                {
                                    PrivMsg(channel, String.Format("{0} is already dead !", sender));
                                }
                                else
                                {
                                    // CRITICAL!
                                    int dmgType = generator.Next(100);
                                    double dmgCoef = 0;
                                    int base_dmg = 0;
                                    if (who_class.Equals("str"))
                                    {
                                        dmgCoef = settings.strhpkoef;
                                        base_dmg = (int)(who_level * settings.strhpkoef + settings.strbasehp / 5);
                                    }
                                    else if (who_class.Equals("agi"))
                                    {
                                        dmgCoef = settings.agihpkoef;
                                        base_dmg = (int)(who_level * settings.agihpkoef + settings.agibasehp / 5);
                                    }
                                    else if (who_class.Equals("int"))
                                    {
                                        dmgCoef = settings.inthpkoef;
                                        base_dmg = (int)(who_level * settings.inthpkoef + settings.intbasehp / 5);
                                    }
                                    // 80% chance to attack
                                    if (dmgType >= 0 && dmgType <= 79)
                                    {
                                        double dmgMax = who_level * dmgCoef * 2;
                                        int dmgMaxRound = (int)Math.Round(dmgMax);
                                        int dmg = base_dmg
                                                        + generator.Next(dmgMaxRound)
                                                        + 1;
                                        int sender_new_hp;

                                        if ((sender_hp - dmg) <= 0)
                                        {
                                            sender_new_hp = 0;
                                        }
                                        else
                                        {
                                            sender_new_hp = sender_hp - dmg;
                                        }
                                        query = String.Format("UPDATE irc_qog SET hp = {0} WHERE id ={1}", sender_new_hp, sender_id);
                                        dc.sendUpdateQuery(query);
                                        PrivMsg(channel, String.Format("{0} strikes back {1}{2}{3} with {4} for {5} damage.", who, Colors.GREEN, sender, Colors.NORMAL, with, dmg));
                                        if (sender_new_hp == 0)
                                        {
                                            int xpgain = GetXp(sender_level);
                                            PrivMsg(channel, String.Format("{0} strikes back and {1}KILLED {2}{3} and gained {4} xp !! (total {5})", who, Colors.RED, Colors.NORMAL, sender, xpgain, (xpgain + who_xp)));

                                            query = String.Format("UPDATE irc_qog SET xp = xp + {0} WHERE id ={1}", xpgain, who_id);
                                            dc.sendUpdateQuery(query);
                                        }
                                        DecreaseStamina(who, settings.stamina_attack);
                                    }
                                    // 5% change to critical
                                    else if (dmgType >= 80 && dmgType <= 84)
                                    {
                                        double dmgMax = who_level * dmgCoef * 2;
                                        int dmgMaxRound = (int)Math.Round(dmgMax);
                                        int dmg = (base_dmg
                                                        + generator.Next(dmgMaxRound) + 1) * 2;
                                        int sender_new_hp;
                                        if ((sender_hp - dmg) <= 0)
                                        {
                                            sender_new_hp = 0;
                                        }
                                        else
                                        {
                                            sender_new_hp = sender_hp - dmg;
                                        }
                                        query = String.Format("UPDATE irc_qog SET hp = {0} WHERE id ={1}", sender_new_hp, sender_id);
                                        dc.sendUpdateQuery(query);
                                        PrivMsg(channel, String.Format("{0} critically strikes back {1}{2}{3} with {4} for {5} damage !", who, Colors.GREEN, sender, Colors.NORMAL, with, dmg));
                                        if (sender_new_hp == 0)
                                        {
                                            int xpgain = GetXp(sender_level);
                                            PrivMsg(channel, String.Format("{0} strikes back and {1}KILLED {2}{3} and gained {4} xp !! (total {5})", who, Colors.RED, Colors.NORMAL, sender, xpgain, (xpgain + who_xp)));
                                            query = String.Format("UPDATE irc_qog SET xp = xp + {0} WHERE id ={1}", xpgain, who_id);
                                            dc.sendUpdateQuery(query);

                                        }
                                        DecreaseStamina(who, settings.stamina_attack);
                                    }
                                    // 10% miss
                                    else if (dmgType >= 85 && dmgType <= 94)
                                    {
                                        PrivMsg(channel, String.Format("{0} missed !", who));
                                        DecreaseStamina(who, settings.stamina_attack);
                                    }
                                    // 5% critical miss
                                    else if (dmgType >= 95 && dmgType <= 99)
                                    {
                                        double dmgMax = who_level * dmgCoef
                                                        * 0.5f;
                                        int dmgMaxRound = (int)Math.Round(dmgMax);
                                        int dmg = base_dmg
                                                        + generator.Next(dmgMaxRound)
                                                        + 1;
                                        int who_new_hp;

                                        if ((who_hp - dmg) <= 0)
                                        {
                                            who_new_hp = 0;
                                        }
                                        else
                                        {
                                            who_new_hp = who_hp - dmg;
                                        }
                                        query = String.Format("UPDATE irc_qog SET hp = {0} WHERE id ={1}", who_new_hp, who_id);
                                        dc.sendUpdateQuery(query);
                                        PrivMsg(channel,
                                                        String.Format("{0} critically missed and hits himself for {1} damage !", who, dmg));
                                        if (who_new_hp == 0)
                                        {
                                            PrivMsg(channel,
                                                            String.Format("{0}{1} KILLED HIMSELF .. HE STUPIDO !!{2}", Colors.RED, who, Colors.NORMAL));
                                        }
                                        DecreaseStamina(who, settings.stamina_attack);
                                    }
                                }
                                ///////////////////////////////////////////////////
                            }
                        }
                    }
                }
                else
                {
                    PrivMsg(channel, String.Format("[{0}bad syntax{1}] attack {2}<who>{1} with {2}<what>", Colors.RED, Colors.NORMAL, Colors.GREEN));
                }
                return;
            }
            catch(Exception e)
            {
                Console.WriteLine(String.Format("{0}\n{1}", e.Message, e.StackTrace));
            }*/
        }

        private void Message_Cast(string message, string channel, string sender)
        {
            try
            {
                if (settings.noobs[settings.noobs.IndexOf(sender)].cooldown > 0)
                {
                    PrivMsg(channel, string.Format("{0}s cooldown remaining!", settings.noobs[settings.noobs.IndexOf(sender)].cooldown / 1000));
                    return;
                }
                String[] cmds = message.Split(' ');
                if (cmds.Length > 3 && cmds[2].Equals("at", StringComparison.OrdinalIgnoreCase))
                {
                    String who = cmds[3];
                    String spell = cmds[1];
                    bool jenachane = false;
                    foreach (ChannelUser u in irc.GetChannel(channel).Users.Values)
                    {
                        if (u.Nick.Equals(who, StringComparison.OrdinalIgnoreCase))
                            jenachane = true;
                    }
                    if (!jenachane)
                    {
                        return;
                    }
                    String query = String.Format("SELECT id, level, hp, mana, class, stamina, xp FROM irc_qog WHERE nick = '{0}'", sender);
                    SQLiteDataReader rs = dc.sendQuery(query);
                    int sender_id = 0;
                    int sender_level = 0;
                    int sender_hp = 0;
                    int sender_mana = 0;
                    float sender_stamina = 0.0f;
                    int sender_xp = 0;
                    String sender_class = "";
                    while (rs.Read())
                    {
                        sender_id = rs.GetInt32(0);
                        sender_level = rs.GetInt32(1);
                        sender_hp = Convert.ToInt32(rs["hp"]);
                        sender_mana = Convert.ToInt32(rs["mana"]);
                        sender_class = rs["class"].ToString();
                        sender_stamina = rs.GetFloat(5);
                        sender_xp = rs.GetInt32(6);
                    }
                    rs.Close();
                    if (sender_hp == 0 || string.IsNullOrEmpty(sender_class)) return;
                    if (sender_id != 0)
                    {
                        query = String.Format("SELECT id, level, hp, mana, class, stamina, xp FROM irc_qog WHERE nick ='{0}'", who);
                        rs = dc.sendQuery(query);
                        int who_id = 0;
                        int who_level = 0;
                        int who_hp = 0;
                        int who_mana = 0;
                        float who_stamina = 0.0f;
                        int who_xp = 0;
                        String who_class = "";
                        while (rs.Read())
                        {
                            who_id = rs.GetInt32(0);
                            who_level = rs.GetInt32(1);
                            who_hp = Convert.ToInt32(rs["hp"]);
                            who_mana = Convert.ToInt32(rs["mana"]);
                            who_class = rs["class"].ToString();
                            who_stamina = rs.GetFloat(5);
                            who_xp = rs.GetInt32(6);
                        }
                        rs.Close();
                        if (who_hp <= 0)
                        {
                            PrivMsg(channel, String.Format("{0} is already dead !", who));
                        }
                        else
                        {
                            int dmg = 0, stamina_dmg = 0, mana_dmg = 0, cost = 0;
                            bool jespell = false;
                            Spell spel = null;
                            foreach (Spell spl in settings.spells)
                            {
                                if (spl.Name.Equals(spell))
                                {
                                    if (sender.Equals(who, StringComparison.OrdinalIgnoreCase) && !spl.Onself)
                                    {
                                        PrivMsg(channel, string.Format("you cannot cast {0} on yourself!", spl.Name));
                                        return;
                                    }
                                    spl.Damaz(sender_level, out dmg, out mana_dmg, out stamina_dmg, out cost);
                                    jespell = true;
                                    spel = spl;
                                }
                            }
                            if (!jespell)
                            {
                                PrivMsg(channel, string.Format("spell {0} does not exist!", spell));
                                return;
                            }
                            if (sender_mana < cost)
                            {
                                PrivMsg(channel, string.Format("you are low on mana, {0} is required!", cost));
                                return;
                            }
                            int who_new_hp, who_new_mana, who_new_stamina;

                            if ((who_hp - dmg) <= 0)
                            {
                                who_new_hp = 0;
                            }
                            else
                            {
                                who_new_hp = who_hp - dmg;
                            }
                            if ((who_mana - mana_dmg) <= 0)
                            {
                                who_new_mana = 0;
                            }
                            else
                            {
                                who_new_mana = who_mana - mana_dmg;
                            }
                            if ((who_stamina - stamina_dmg) <= 0)
                            {
                                who_new_stamina = 0;
                            }
                            else
                            {
                                who_new_stamina = (int)who_stamina - stamina_dmg;
                            }
                            query = String.Format("UPDATE irc_qog SET hp = {0} WHERE id ={1}", who_new_hp, who_id);
                            dc.sendUpdateQuery(query);
                            query = String.Format("UPDATE irc_qog SET mana = {0} WHERE id ={1}", who_new_mana, who_id);
                            dc.sendUpdateQuery(query);
                            query = String.Format("UPDATE irc_qog SET stamina = {0} WHERE id ={1}", who_new_stamina, who_id);
                            dc.sendUpdateQuery(query);
                            query = String.Format("UPDATE irc_qog SET mana = MAX(mana - {0},0) WHERE id ={1}", cost, sender_id);
                            dc.sendUpdateQuery(query);
                            PrivMsg(channel, String.Format("{0} casts {1}{2}{3} at {4} for {5} hp damage, {6} mana damage and {7} stamina damage", sender, Colors.GREEN, spell, Colors.NORMAL, who, dmg, mana_dmg, stamina_dmg));
                            if (who.Equals(sender) && who_new_hp == 0)
                            {
                                PrivMsg(channel,
                                                String.Format("{0}{1} KILLED HIMSELF .. HE STUPIDO !!{2}", Colors.RED, sender, Colors.NORMAL));
                                return;
                            }
                            if (who_new_hp == 0)
                            {
                                int xpgain = GetXp(who_level);
                                PrivMsg(channel, String.Format("{0} {1}KILLED {2}{3} and gained {4} xp!! (total {5})", sender, Colors.RED, Colors.NORMAL, who, xpgain, (xpgain + sender_xp)));
                                query = String.Format("UPDATE irc_qog SET xp = xp + {0} WHERE id ={1}", xpgain, sender_id);
                                dc.sendUpdateQuery(query);
                            }
                            settings.noobs[settings.noobs.IndexOf(sender)].cooldown = spel.Cooldown;
                        }
                    }
                }
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("{0}\n{1}", e.Message, e.StackTrace));
            }
        }

        private void Message_QQ(string message, string channel)
        {
            String[] smes = message.Trim().Split(' ');
            String term = "";
            for (int i = 1; i < smes.Length; i++)
                term += String.Format("{0} ", smes[i]);
            String query = "SELECT ID FROM irc_learn_words WHERE Term = @name";
            SQLiteDataReader rs = dc.sendQuery(query,term);
            int id = 0;
            while (rs.Read())
            {
                id = rs.GetInt32(0);
            }
            rs.Close();
            if (id != 0)
            {
                query = "SELECT definition FROM irc_learn_definitions WHERE WID = @name";
                rs = dc.sendQuery(query,id);
                String defmessage = "";
                while (rs.Read())
                {
                    defmessage += String.Format("{0}; ", rs["definition"]);
                }
                rs.Close();
                PrivMsg(channel, String.Format("{0}{1}{0}: {2}", Colors.BOLD, term, defmessage));
            }
            else
            {
                query = "SELECT ID FROM irc_learn_words WHERE Term = @name";
                rs = dc.sendQuery(query, smes[1]);
                id = 0;
                while (rs.Read())
                {
                    id = rs.GetInt32(0);
                }
                rs.Close();
                if (id != 0)
                {
                    query = String.Format("SELECT definition FROM irc_learn_definitions WHERE WID = {0}", id);
                    rs = dc.sendQuery(query);
                    String defmessage = "";
                    while (rs.Read())
                    {
                        defmessage += String.Format("{0}; ", rs["definition"]);
                        if (defmessage.Length > 420)
                        {
                            PrivMsg(channel, String.Format("{0}{1}{0}: {2}", Colors.BOLD, term, defmessage));
                            defmessage = "";
                        }
                    }
                    rs.Close();
                    PrivMsg(channel, String.Format("{0}{1}{0}: {2}", Colors.BOLD, term, defmessage));
                }
            }
        }

        private void Message_QQ_text(string message, string channel, string sender)
        {
            String[] smes = message.Split(new string[] { "?? " }, StringSplitOptions.None);
            String[] smes2;
            int lengt=smes.Length;
            for (int i = 1; i < lengt; i++)
            {
                smes2 = smes[i].Split(' ');
                String term = "";
                term = String.Format("{0} ", smes2[0]);
                String query = "SELECT ID FROM irc_learn_words WHERE Term = @name";
                SQLiteDataReader rs = dc.sendQuery(query, term);
                int id = 0;
                while (rs.Read())
                {
                    id = rs.GetInt32(0);
                }
                rs.Close();
                if (id != 0)
                {
                    query = String.Format("SELECT definition FROM irc_learn_definitions WHERE WID = {0}", id);
                    rs = dc.sendQuery(query);
                    String defmessage = "";
                    while (rs.Read())
                    {
                        defmessage += String.Format("{0}; ", rs["definition"]);
                        if (defmessage.Length > 420)
                        {
                            PrivMsg(channel, String.Format("{0}{1}{0}: {2}", Colors.BOLD, term, defmessage));
                            defmessage = "";
                        }
                    }
                    rs.Close();
                    PrivMsg(channel, String.Format("{0}{1}{0}: {2}", Colors.BOLD, term, defmessage));
                }
            }
        }

        private void Message_Unlearn(string message, string channel, string sender)
        {
            if (settings.noobs[settings.noobs.IndexOf(sender)].level < 7)
            {
                PrivMsg(channel, "insufficient kredencials!");
                return;
            }
            String[] smes = message.Split(new string[] { " = " }, StringSplitOptions.None);
            String term = "";
            string def = "";
            term = smes[0].Remove(0, 9);
            term = String.Format("{0} ", term);
            def = smes[smes.Length - 1];
            String query = "SELECT ID FROM irc_learn_words WHERE Term = @name";
            using (SQLiteDataReader rs = dc.sendQuery(query, term))
            {
                int id = 0;
                while (rs.Read())
                {
                    id = rs.GetInt32(0);
                }
                rs.Close();
                if (id != 0)
                {
                    query = String.Format("DELETE FROM irc_learn_definitions WHERE WID = {0} AND definition = @name", id);
                    dc.sendUpdateQuery(query, String.Format("{0} ", smes[1]));
                    PrivMsg(channel, "unlearned!");
                }
            }
        }

        private void Message_Learn(string message, string channel, string sender)
        {
            if (settings.noobs[settings.noobs.IndexOf(sender)].level < 0)
            {
                PrivMsg(channel, "insufficient kredencials!");
                return;
            }
            String what = "";
            String def = "";
            String[] smes1 = message.Split(new string[] { " = " }, StringSplitOptions.None);
            if (smes1.Length == 2)
            {
                String[] smes2 = smes1[0].Split(' ');
                String[] smes3 = smes1[1].Split(' ');
                if (smes2.Length >= 2)
                {
                    for (int i = 1; i < smes2.Length; i++)
                        what += String.Format("{0} ", smes2[i]);
                    for (int i = 0; i < smes3.Length; i++)
                    {
                        def += String.Format("{0} ", smes3[i]);
                    }
                    String query = "SELECT ID FROM irc_learn_words where Term = @name";
                    SQLiteDataReader rs = dc.sendQuery(query, what);
                    int id = 0;
                    while (rs.Read())
                    {
                        id = rs.GetInt32(0);
                    }
                    rs.Close();
                    if (id == 0)
                    {
                        query = "INSERT INTO irc_learn_words VALUES (null, @name)";
                        dc.sendUpdateQuery(query, what);
                        query = "SELECT ID FROM irc_learn_words where Term = @name";
                        rs = dc.sendQuery(query, what);
                        while (rs.Read())
                        {
                            id = rs.GetInt32(0);
                        }
                    }
                    rs.Close();
                    def = def.Replace("\\0", "");
                    query = "INSERT INTO irc_learn_definitions VALUES (@name1, @name2, @name3)";
                    dc.sendUpdateQuery(query, id, def, sender);
                    PrivMsg(channel, "ok definicia pridana :p");
                }
            }
        }
        private void Message_QOG(string message, string channel, string sender)
        {
            String[] cmds = message.Split(' ');
            if (cmds.Length > 1)
            {
                const String query = "select qog, level from irc_qog where nick = @name";
                SQLiteDataReader rs;
                rs = dc.sendQuery(query, cmds[1]);
                if (rs.Read())
                    PrivMsg(channel, String.Format("{0}{1}{0}: level {2} qog {3}", Colors.BOLD, cmds[1], rs.GetInt32(1), rs.GetFloat(0)));
                else
                    PrivMsg(channel, String.Format("{0}{1}{0}: level 1 qog 0.0", Colors.BOLD, cmds[1]));
                rs.Close();
            }
            else
            {
                String query = String.Format("select qog, level from irc_qog where nick = '{0}'", sender);
                SQLiteDataReader rs;
                rs = dc.sendQuery(query);
                if (rs.Read())
                    PrivMsg(channel, String.Format("{0}{1}{0}: level {2} qog {3}", Colors.BOLD, sender, rs.GetInt32(1), rs.GetFloat(0)));
                else
                    PrivMsg(channel, String.Format("{0}{1}{0}: level 1 qog 0.0", Colors.BOLD, sender));
                rs.Close();
            }
        }
        private void Message_QOGstats(string message, string channel, string sender)
        {
            using (SQLiteDataReader rs = dc.sendQuery("select qog, level, nick from irc_qog order by level desc, qog desc"))
            {
                String mesage = "";
                int i = 1;
                while (rs.Read())
                {
                    mesage += String.Format("{0}" + i + ": {1}{0}: level {2} qog {3}; ", Colors.BOLD, rs["nick"], rs.GetInt32(1), rs.GetFloat(0));
                    i++;
                    if (i > 5)
                        break;
                }
                rs.Close();
                PrivMsg(channel, mesage);
            }
        }

        private void Message_roll(string message, string channel, string sender)
        {
            PrivMsg(channel, generator.Next(101).ToString());
        }

        private void Message_rickroll(string message, string channel, string sender)
        {
            int i = generator.Next(6);
            String mes = "";
            switch (i)
            {
                case 0:
                    mes = "Never gonna give you up";
                    break;
                case 1:
                    mes = "Never gonna let you down";
                    break;
                case 2:
                    mes = "Never gonna run around and desert you";
                    break;
                case 3:
                    mes = "Never gonna make you cry";
                    break;
                case 4:
                    mes = "Never gonna say goodbye";
                    break;
                case 5:
                    mes = "Never gonna tell a lie and hurt you";
                    break;
                default:
                    mes = "bluglafnuf";
                    break;
            }
            PrivMsg(channel, mes);
        }

        private void Message_Addgreet(string message, string channel)
        {
            message = message.Trim();
            String[] cmds = message.Split(' ');
            if (cmds.Length > 2)
            {
                String greet = "";
                for (int i = 1; i < cmds.Length - 1; i++)
                {
                    greet += String.Format("{0} ", cmds[i]);
                }

                String usr = cmds[cmds.Length - 1];
                greet = greet.Replace("\\0", "");
                const String query = "INSERT INTO irc_greetings VALUES (null, @name, @name2)";
                dc.sendUpdateQuery(query, greet, usr);
                PrivMsg(channel, "ok pozdrav pridany :>");
            }
        }
        private void Message_Rose(string message, string channel, string sender)
        {
            if(message.Split(' ').Length<2) return;
            irc.SendMessage(SendType.Action, channel, String.Format("{0} offers this {1}@{2}}}-,-'--{3} to the beautiful {4}  :)", sender, Colors.RED, Colors.GREEN, Colors.NORMAL, message.Split(' ')[1]));
        }
        private void Message_Penis(string message, string channel, string sender)
        {
            if (message.Split(' ').Length < 2) return;
            irc.SendMessage(SendType.Action, channel, String.Format("{0} stuffed this big black {1}<=========8{1} into the beautiful {2}  :)", sender, Colors.BOLD, message.Split(' ')[1]));
        }
        private void Message_Quote(string message, string channel)
        {
            String[] cmds = message.ToLower().Split(' ');
            if (cmds.Length <= 1)
            {
                using (SQLiteDataReader rs = dc.sendQuery("SELECT quote, user FROM irc_quotes ORDER BY RANDOM() LIMIT 1"))
                {
                    String ar = "";
                    while (rs.Read())
                    {
                        ar = String.Format("{0}quote: {0}{1} (c) {2}", Colors.BOLD, rs["quote"].ToString().Trim(), rs["user"].ToString()).Trim();
                    }
                    rs.Close();
                    PrivMsg(channel, !string.IsNullOrEmpty(ar) ? ar : "nic tam neni luma!");
                }
            }
            else if (cmds.Length > 1)
            {
                string kvoota = "";
                for (int i = 1; i < cmds.Length; i++)
                {
                    kvoota += String.Format("{0} ", cmds[i]);
                }
                kvoota = kvoota.Trim();
                // String query =
                // "SELECT quote, user FROM irc_quotes WHERE MATCH (quote, user) AGAINST ('"+cmds[1]+"')";
                const String query = "SELECT quote, user FROM irc_quotes WHERE LOWER(quote) LIKE @kvoota OR LOWER(user) LIKE @kvoota ORDER BY RANDOM() LIMIT 1";
                using (SQLiteDataReader rs = dc.sendQuery2(query, kvoota))
                {
                    String ar = "";
                    while (rs.Read())
                    {
                        ar = String.Format("{0}quote: {0}{1} (c) {2}", Colors.BOLD, rs["quote"].ToString().Trim(), rs["user"].ToString()).Trim();
                    }
                    rs.Close();
                    PrivMsg(channel, !string.IsNullOrEmpty(ar) ? ar : "nic tam neni luma!");
                }
            }
        }

        private void Message_Quotefrom(string message, string channel)
        {
            String[] cmds = message.ToLower().Split(' ');
            if (cmds.Length > 1)
            {
                // String query =
                // "SELECT quote, user FROM irc_quotes WHERE MATCH (quote, user) AGAINST ('"+cmds[1]+"')";
                const String query = "SELECT quote, user FROM irc_quotes WHERE LOWER(user) LIKE @kvoota ORDER BY RANDOM() LIMIT 1";
                using (SQLiteDataReader rs = dc.sendQuery2(query, cmds[1]))
                {
                    String ar = "";
                    while (rs.Read())
                    {
                        ar = String.Format("{0}quote: {0}{1} (c) {2}", Colors.BOLD, rs["quote"].ToString().Trim(), rs["user"].ToString()).Trim();
                    }
                    rs.Close();
                    PrivMsg(channel, !string.IsNullOrEmpty(ar) ? ar : "nic tam neni luma!");
                }
            }
        }
        private void Message_Quotecount(string message, string channel)
        {
            String[] cmds = message.ToLower().Split(' ');
            if (cmds.Length > 1)
            {
                // String query =
                // "SELECT quote, user FROM irc_quotes WHERE MATCH (quote, user) AGAINST ('"+cmds[1]+"')";
                const String query = "select count(*) from(SELECT quote, user FROM irc_quotes WHERE LOWER(quote) LIKE @kvoota OR LOWER(user) LIKE @kvoota)";
                using (SQLiteDataReader rs = dc.sendQuery2(query, message.Remove(0, 12).Trim()))
                {
                    String ar = "";
                    rs.Read();
                    ar = String.Format("quotecount of {0}{1}{0}: {2}", Colors.BOLD, message.Remove(0, 12).Trim(), rs[0]);
                    rs.Close();
                    PrivMsg(channel, ar);
                }
            }
        }
        private void Message_Salat(string channel, string sender)
        {
            if (generator.Next(100) > 15)
            {
                int i = generator.Next(22);
                String mes = "";
                switch (i)
                {
                    case 0: mes = "no co zlato"; break;
                    case 1: mes = "zase ma ohovaraju salamovia"; break;
                    case 2: mes = "pritomen!"; break;
                    case 3: mes = "poslusne hlasim ze za mojej sluzby sa nic zvlastne nestalo, sudruh kapitan!"; break;
                    case 4: mes = "?"; break;
                    case 5: mes = "\\o/"; break;
                    case 6: mes = "neber meno bozie do tlamy nadarmo!"; break;
                    case 7:
                        mes = "lalala";
                        break;
                    case 8:
                        mes = "ano ja viem ze som popularny";
                        break;
                    case 9:
                        mes = String.Format("a poznate http://{0}.istheshit.net/ ?", sender);
                        break;
                    case 10:
                        mes = "ale fakt mam rad (o)(o) :>";
                        break;
                    case 11:
                        mes = "nic nebude!";
                        break;
                    case 12:
                        mes = "ano?";
                        break;
                    case 13:
                        mes = "hmm... nie";
                        break;
                    case 14:
                        mes = "koho? ake lietadlo?";
                        break;
                    case 15:
                        mes = "som na zachode, nemluvit...";
                        break;                 
                    case 16:
                        mes = "co zas?";
                        break;
                    case 17:
                        mes = "mam pre teba len 3 slova... uz nikdy nehadz perly sviniam! ehm...";
                        break;
                    case 18:
                        mes = "...a vtedy to na mna cele spadlo...";
                        break;
                    case 19:
                        mes = "klop klop... kto tam? blby bot... X-D";
                        break;
                    case 20:
                        mes = String.Format(">:O FU {0} !!!", sender);
                        break;
                    case 21:
                        irc.SendMessage(SendType.Action, channel, "12 73h 1337 b07");
                        return;
                    default:
                        mes = "bluglafnuf";
                        break;
                }
                PrivMsg(channel, mes);
            }
        }

        private void Message_yelling(string channel, string sender)
        {
            if (generator.Next(100) > 60)
            {
                int i = generator.Next(9);
                String mes = "";
                switch (i)
                {
                    case 0:
                        mes = "cixo buc!";
                        break;
                    case 1:
                        mes = "nehuc tak!";
                        break;
                    case 2:
                        mes = "ides ho!?";
                        break;
                    case 3:
                        mes = "nebud zly!";
                        break;
                    case 4:
                        mes = String.Format("{0}: nebliakaj!", sender);
                        break;
                    case 5:
                        mes = "bez vykricnikov sa neda?";
                        break;
                    case 6:
                        mes = String.Format("{0}: zas si vulgarny?", sender);
                        break;
                    case 7:
                        mes = "znizujete miestnu uroven?";
                        break;
                    case 8:
                        mes = "FFFFFFFFFFFFFFFFFFUUUUUUUUUUUUUUUUUUUUUUUUUUUU-";
                        break;
                    default:
                        mes = "bluglafnuf";
                        break;
                }
                PrivMsg(channel, mes);
            }
        }

        private void Message_Plackovia(string channel, string sender)
        {
            if (generator.Next(100) > 80)
            {
                int i = generator.Next(8);
                String mes = "";
                switch (i)
                {
                    case 0:
                        mes = ":(";
                        break;
                    case 1:
                        mes = "D:";
                        break;
                    case 2:
                        mes = "este sa rozplacem";
                        break;
                    case 3:
                        mes = ":_______(";
                        break;
                    case 4:
                        mes = String.Format("to prejde, {0}!! :)", sender);
                        break;
                    case 5:
                        mes = "hmm :<";
                        break;
                    case 6:
                        mes = "to nastve";
                        break;
                    case 7:
                        mes = "nebul!";
                        break;
                    default:
                        mes = "bluglafnuf";
                        break;
                }
                PrivMsg(channel, mes);
            }
        }

        private void Message_Smajle(string channel)
        {
            if (generator.Next(100) > 80)
            {
                int i = generator.Next(26);
                String mes = "";
                switch (i)
                {
                    case 0:
                        mes = ":D";
                        break;
                    case 1:
                        mes = ":F";
                        break;
                    case 2:
                        mes = "to su mi smiechoty";
                        break;
                    case 3:
                        mes = "no len sa tolko negeb";
                        break;
                    case 4:
                        mes = "roflmao";
                        break;
                    case 5:
                        mes = "skor tragikomicke";
                        break;
                    case 6:
                        mes = ":p";
                        break;
                    case 7:
                        mes = "stfu n00b :p";
                        break;
                    case 8:
                        mes = "mvg!";
                        break;
                    case 9:
                        mes = "you remind me of gay...";
                        break;
                    case 10:
                        mes = "ke?";
                        break;
                    case 11:
                        mes = ":)";
                        break;
                    case 12:
                        mes = "x.x";
                        break;
                    case 13:
                        mes = "len sa nepocikaj";
                        break;
                    case 14:
                        mes = "dik, akurat jem...";
                        break;
                    case 15:
                        mes = "do pohodicky typek";
                        break;
                    case 16:
                        mes = "omgwtfbbqhax!";
                        break;
                    case 17:
                        mes = "o.o";
                        break;
                    case 18:
                        mes = "este nieco zadel";
                        break;
                    case 19:
                        mes = "tam chod kodit!";
                        break;
                    case 20:
                        mes = "trapne, vymysli nieco lepsie...";
                        break;
                    case 21:
                        irc.SendMessage(SendType.Action,channel, "je tiez stastny");
                        return;
                    case 22:
                        mes = "ze robis nervy?";
                        break;
                    case 23:
                        mes = "lolka";
                        break;
                    case 24:
                        mes = "jasne moc vtipne :P";
                        break;
                    case 25:
                        mes = "no comment";
                        break;
                    default:
                        mes = "bluglafnuf";
                        break;
                }
                PrivMsg(channel, mes);
            }
        }

        private void Message_Stats(string message, string channel, string sender)
        {
            String[] cmds = message.Split(' ');
            float maxHP = 0, maxMana = 0, maxStamina = 0;
            if (cmds.Length > 1)
            {
                String query = String.Format("SELECT level, hp, mana, class, stamina, xp FROM irc_qog WHERE nick = '{0}'", cmds[1]);
                SQLiteDataReader rs;
                rs = dc.sendQuery(query);
                if (rs.Read())
                {
                    if (rs["class"].ToString().Equals("agi")
                                    || (rs["class"].ToString().Equals("str") || (rs
                                                    ["class"].ToString().Equals("int"))))
                    {
                        if (rs["class"].ToString().Equals("agi"))
                        {
                            maxHP = settings.agihpkoef * settings.hpkoef * Convert.ToInt32(rs["level"]) + settings.agibasehp;
                            maxMana = settings.agimanakoef * settings.manakoef * Convert.ToInt32(rs["level"]) + settings.agibasemana;
                            maxStamina = settings.agistaminakoef * settings.staminakoef * Convert.ToInt32(rs["level"]) + settings.agibasestamina;
                        }
                        if (rs["class"].ToString().Equals("str"))
                        {
                            maxHP = settings.strhpkoef * settings.hpkoef * Convert.ToInt32(rs["level"]) + settings.strbasehp;
                            maxMana = settings.strmanakoef * settings.manakoef * Convert.ToInt32(rs["level"]) + settings.strbasemana;
                            maxStamina = settings.strstaminakoef * settings.staminakoef * Convert.ToInt32(rs["level"]) + settings.strbasestamina;
                        }
                        if (rs["class"].ToString().Equals("int"))
                        {
                            maxHP = settings.inthpkoef * settings.hpkoef * Convert.ToInt32(rs["level"]) + settings.intbasehp;
                            maxMana = settings.intmanakoef * settings.manakoef * Convert.ToInt32(rs["level"]) + settings.intbasemana;
                            maxStamina = settings.intstaminakoef * settings.staminakoef * Convert.ToInt32(rs["level"]) + settings.intbasestamina;
                        }
                        irc.SendMessage(SendType.Message, channel, String.Format("{0}{1}{0}: Level {2} ({3}) [ {4}HP: {5}{6}/{13} |{7} MP: {5}{8}/{14} |{9} Stamina: {5}{10}/{15} |{11} XP: {5}{12}/{16} ]"
                            , Colors.BOLD, cmds[1], Convert.ToInt32(rs["level"]), rs["class"].ToString(), Colors.RED, Colors.NORMAL, Convert.ToInt32(rs["hp"]), Colors.BLUE, Convert.ToInt32(rs["mana"]), Colors.PURPLE, Convert.ToSingle(rs["stamina"]), Colors.YELLOW, Convert.ToInt32(rs["xp"]), Math.Round(maxHP), Math.Round(maxMana), Math.Round(maxStamina), GetLvlupXP(Convert.ToInt32(rs["level"]))));
                    }
                    else
                        PrivMsg(channel, String.Format("{0}{1}{0}: PURE NOOB!", Colors.BOLD, cmds[1]));
                }
                rs.Close();
            }
            else
            {
                String query = String.Format("select * from irc_qog where nick = '{0}'", sender);
                SQLiteDataReader rs;
                rs = dc.sendQuery(query);
                if (rs.Read())
                {
                    if (rs["class"].ToString().Equals("agi")
                                    || (rs["class"].ToString().Equals("str") || (rs
                                                    ["class"].ToString().Equals("int"))))
                    {
                        if (rs["class"].ToString().Equals("agi"))
                        {
                            maxHP = settings.agihpkoef * settings.hpkoef * Convert.ToInt32(rs["level"]) + settings.agibasehp;
                            maxMana = settings.agimanakoef * settings.manakoef * Convert.ToInt32(rs["level"]) + settings.agibasemana;
                            maxStamina = settings.agistaminakoef * settings.staminakoef * Convert.ToInt32(rs["level"]) + settings.agibasestamina;
                        }
                        if (rs["class"].ToString().Equals("str"))
                        {
                            maxHP = settings.strhpkoef * settings.hpkoef * Convert.ToInt32(rs["level"]) + settings.strbasehp;
                            maxMana = settings.strmanakoef * settings.manakoef * Convert.ToInt32(rs["level"]) + settings.strbasemana;
                            maxStamina = settings.strstaminakoef * settings.staminakoef * Convert.ToInt32(rs["level"]) + settings.strbasestamina;
                        }
                        if (rs["class"].ToString().Equals("int"))
                        {
                            maxHP = settings.inthpkoef * settings.hpkoef * Convert.ToInt32(rs["level"]) + settings.intbasehp;
                            maxMana = settings.intmanakoef * settings.manakoef * Convert.ToInt32(rs["level"]) + settings.intbasemana;
                            maxStamina = settings.intstaminakoef * settings.staminakoef * Convert.ToInt32(rs["level"]) + settings.intbasestamina;
                        }
                        irc.SendMessage(SendType.Message, channel, String.Format("{0}{1}{0}: Level {2} ({3}) [ {4}HP: {5}{6}/{13} |{7} MP: {5}{8}/{14} |{9} Stamina: {5}{10}/{15} |{11} XP: {5}{12}/{16} ]"
                            , Colors.BOLD, sender, Convert.ToInt32(rs["level"]), rs["class"], Colors.RED, Colors.NORMAL, Convert.ToInt32(rs["hp"]), Colors.BLUE, Convert.ToInt32(rs["mana"]), Colors.PURPLE, Convert.ToSingle(rs["stamina"]), Colors.YELLOW, Convert.ToInt32(rs["xp"]), Math.Round(maxHP), Math.Round(maxMana), Math.Round(maxStamina), GetLvlupXP(Convert.ToInt32(rs["level"]))));
                    }
                    else
                        PrivMsg(channel, String.Format("{0}{1}{0}: PURE NOOB!", Colors.BOLD, sender));
                }
                rs.Close();
            }

        }

        private void Message_Setclass(string message, string channel, string sender)
        {
            String[] cmds = message.Split(' ');
            String query;
            if ((cmds.Length > 1)
                            && ((cmds[1].Equals("agi") || (cmds[1].Equals("str") || (cmds[1]
                                            .Equals("int"))))))
            {

                query = String.Format("select class, level, nick from irc_qog where nick = '{0}'", sender);
                SQLiteDataReader rs = null;
                rs = dc.sendQuery(query);
                if (rs.Read())
                {
                    if (rs["class"].ToString().Equals("agi")
                                    || (rs["class"].ToString().Equals("str") || (rs
                                                    ["class"].ToString().Equals("int"))))
                    {
                        PrivMsg(channel, String.Format("your class is already set to: {0}", rs["class"].ToString()));
                        rs.Close();
                        return;
                    }
                }
                else
                {
                    query = String.Format("INSERT INTO irc_qog VALUES (null, '{0}', 0, 1,0,0,'',0,0)", sender);
                    dc.sendUpdateQuery(query);
                    query = String.Format("select class, level, nick from irc_qog where nick = '{0}'", sender);
                    rs.Close();
                    rs = dc.sendQuery(query);
                    rs.Read();
                }
                int level = 1;
                level = rs.GetInt32(1);
                query = String.Format("UPDATE irc_qog SET CLASS =  '{0}' where nick = '{1}'", cmds[1], sender);
                dc.sendUpdateQuery(query);
                PrivMsg(channel, String.Format("setting class to: {0}", cmds[1]));
                if (cmds[1].Equals("agi"))
                {
                    query = String.Format("UPDATE irc_qog SET HP = {0}, mana = {1}, stamina = {2} WHERE nick = '{3}'", (settings.agihpkoef * settings.hpkoef * level + settings.agibasehp), (settings.agimanakoef * settings.manakoef * level + settings.agibasemana), (settings.agistaminakoef * settings.staminakoef * level + settings.agibasestamina), sender);
                    dc.sendUpdateQuery(query);
                    PrivMsg(channel, String.Format("{0}{1}{2} has spawned!", Colors.GREEN, rs["nick"].ToString(), Colors.NORMAL));
                }
                if (cmds[1].Equals("int"))
                {
                    query = String.Format("UPDATE irc_qog SET HP = {0}, mana = {1}, stamina = {2} WHERE nick = '{3}'", (settings.inthpkoef * settings.hpkoef * level + settings.intbasehp), (settings.intmanakoef * settings.manakoef * level + settings.intbasemana), (settings.intstaminakoef * settings.staminakoef * level + settings.intbasestamina), sender);
                    dc.sendUpdateQuery(query);
                    PrivMsg(channel, String.Format("{0}{1}{2} has spawned!", Colors.GREEN, rs["nick"].ToString(), Colors.NORMAL));
                }
                if (cmds[1].Equals("str"))
                {
                    query = String.Format("UPDATE irc_qog SET HP = {0}, mana = {1}, stamina = {2} WHERE nick = '{3}'", (settings.strhpkoef * settings.hpkoef * level + settings.strbasehp), (settings.strmanakoef * settings.manakoef * level + settings.strbasemana), (settings.strstaminakoef * settings.staminakoef * level + settings.strbasestamina), sender);
                    dc.sendUpdateQuery(query);
                    PrivMsg(channel, String.Format("{0}{1}{2} has spawned!", Colors.GREEN, rs["nick"].ToString(), Colors.NORMAL));
                }
                rs.Close();
            }
        }
        private void Message_Spells(string message, string channel)
        {
            string mesuge="Availiable spells: ";
            foreach (Spell spell in settings.spells)
            {
                mesuge = String.Format("{0}{1}, ", mesuge, spell.Name);
            }
            PrivMsg(channel, mesuge);
        }

        private void Message_Learned(string channel, string sender)
        {
            if (settings.noobs[settings.noobs.IndexOf(sender)].level < 7)
            {
                PrivMsg(channel, "insufficient kredencials!");
                return;
            }

            if (DateTime.Now.Subtract(lastlearned).TotalHours < 24) return;
            using (SQLiteDataReader rs = dc.sendQuery("SELECT term from irc_learn_words"))
            {
                String quota = "";
                while (rs.Read())
                {
                    if (quota.Length < 440)
                        quota += rs["term"].ToString().Trim() + "; ";
                    else
                    {
                        PrivMsg(channel, quota);
                        quota = rs["term"].ToString().Trim() + "; ";
                    }
                }
                rs.Close();
                if (quota.Length > 0)
                    PrivMsg(channel, quota);
            }
            lastlearned = DateTime.Now;
        }
        private void Message_Fefo(string message, string channel)
        {
            if (generator.Next(100) > 10)
            {
                int i = generator.Next(10);
                String mes = "";
                switch (i)
                {
                    case 0:
                        mes = "fefo: buc ticho!";
                        break;
                    case 1:
                        mes = "D:";
                        break;
                    case 2:
                        mes = "fefo: zas tu iritujes ludi?";
                        break;
                    case 3:
                        mes = "fefo: zmlkni!";
                        break;
                    case 4:
                        mes = "fefo: neotravuj tu vzduch!";
                        break;
                    case 5:
                        mes = "fefo: mlc uz!!";
                        break;
                    case 6:
                        mes = "fefo: nikoho to nezaujima...";
                        break;
                    case 7:
                        mes = "fefo: zas hovoris ked sa ta nikto nepytal??";
                        break;
                    case 8:
                        mes = "fefo: stichnes uz konecne?";
                        break;
                    case 9:
                        mes = "fefo: aha";
                        break;
                    default:
                        mes = "bluglafnuf";
                        break;
                }
                PrivMsg(channel, mes);
            }
        }
        private void Message_Honenei(string channel, Hashtable hashtable)
        {
            if (channel != "#fiit-dota") return;
            string message="";
            foreach (ChannelUser user in hashtable.Values) message += user.Nick + " ";
            PrivMsg(channel, message);
        }
        private void Message_Kick(string message, string channel,string sender)
        {
            if (settings.noobs[settings.noobs.IndexOf(sender)].level < 5)
            {
                PrivMsg(channel, "insufficient kredencials!");
            }
            else
            {
                String[] cmds = message.ToLower().Split(' ');
                if (cmds.Length <= 1)
                {
                    return;
                }
                else
                {
                    if (cmds[1].Equals(irc.Nickname, StringComparison.OrdinalIgnoreCase))
                    {
                        PrivMsg(channel, "aha");
                    }
                    else
                    {
                        irc.RfcKick(channel, cmds[1],cmds.Length<=2 ? "ttfn" : message.Remove(0,7+cmds[1].Length));
                    }
                }
            }
        }
        private void Message_Fajci(string channel, string sender)
        {
            if (!settings.noobs[settings.noobs.IndexOf(sender)].sucked)
            {
                irc.SendMessage(SendType.Action, channel, ":O<=====8");
                irc.SendMessage(SendType.Action, channel, ":O====8");
                irc.SendMessage(SendType.Action, channel, ":O=8");
                irc.SendMessage(SendType.Action, channel, ":O====8");
                irc.SendMessage(SendType.Action, channel, ":O<=====8");
                settings.noobs[settings.noobs.IndexOf(sender)].sucked = true;
            }
        }
        private void Message_Opme(string channel, string sender)
        {
            if (settings.noobs[settings.noobs.IndexOf(sender)].level < 5)
            {
                PrivMsg(channel, "insufficient kredencials!");
            }
            else
            {
                irc.Op(channel, sender);
            }
        }

        private void Message_Level(string channel, string sender, string message)
        {
            PrivMsg(channel, "your kredencial level: " + settings.noobs[settings.noobs.IndexOf(sender)].level);
        }

        private void Message_SetLevel(string channel, string sender, string message)
        {
            string[] cmds=message.Split(' ');
            if (settings.noobs[settings.noobs.IndexOf(sender)].level < 10)
            {
                PrivMsg(channel, "insufficient kredencials!");
            }
            else
            {
                try
                {
                    if (settings.noobs.Contains(cmds[1]))
                    {
                        settings.noobs[settings.noobs.IndexOf(cmds[1])].level = Convert.ToInt32(cmds[2]);
                        if (settings.userlist.ContainsKey(cmds[1])) settings.userlist[cmds[1]] = String.Format("{0} {1}", cmds[2], settings.userlist[cmds[1]].Split(' ')[1]);
                        settings.SaveAuthUsers();
                    }
                }
                catch { return; }
            }
        }
        private void Message_Commands(string channel, int p)
        {
            string mes="";
            int n = (p > 10) ? 10 : p;
            for (int i = 0; (i <= n); i++)
            {
                mes += settings.commandlist[i] + "; ";
            }
            PrivMsg(channel, mes);
        }
        private void Message_Op(string channel, string sender, string message)
        {
            if (settings.noobs[settings.noobs.IndexOf(sender)].level < 5)
            {
                PrivMsg(channel, "insufficient kredencials!");
            }
            else
            {
                String[] cmds = message.ToLower().Split(' ');
                if (cmds.Length <= 1)
                {
                    return;
                }
                else
                {
                    if (cmds[1].Equals(irc.Nickname, StringComparison.OrdinalIgnoreCase))
                    {
                        PrivMsg(channel, "aha");
                    }
                    else
                    {
                        irc.Op(channel, cmds[1]);
                    }
                }
            }
        }
        private void Message_Deop(string channel, string sender, string message)
        {
            if (settings.noobs[settings.noobs.IndexOf(sender)].level < 5)
            {
                PrivMsg(channel, "insufficient kredencials!");
            }
            else
            {
                String[] cmds = message.ToLower().Split(' ');
                if (cmds.Length <= 1)
                {
                    return;
                }
                else
                {
                    if (cmds[1].Equals(irc.Nickname, StringComparison.OrdinalIgnoreCase))
                    {
                        PrivMsg(channel, "aha");
                    }
                    else
                    {
                        try
                        {
                            if (settings.noobs[settings.noobs.IndexOf(sender)].level >= settings.noobs[settings.noobs.IndexOf(cmds[1])].level)
                                irc.Deop(channel, cmds[1]);
                            else PrivMsg(channel, "your kredencial level si lower than target!");
                        }
                        catch { }
                    }
                }
            }
        }
        private void Message_Voiceme(string channel, string sender)
        {
            if (settings.noobs[settings.noobs.IndexOf(sender)].level < 1)
            {
                PrivMsg(channel, "insufficient kredencials!");
            }
            else
            {
                irc.Voice(channel, sender);
            }
        }
        private void Message_Id(string channel, string sender)
        {
            if (settings.noobs[settings.noobs.IndexOf(sender)].level < 2)
            {
                PrivMsg(channel, "insufficient kredencials!");
            }
            else
            {
                List<string> nicks = new List<string>();
                foreach(ChannelUser user in irc.GetChannel(channel).Users.Values)nicks.Add(user.Nick);
                StringBuilder message=new StringBuilder("");
                foreach (Noob nab in settings.noobs)
                {
                    if (nicks.Contains(nab.name) && nab.level > 0) message.Append(String.Format("{0}: level {1}; ", nab.name, nab.level));
                }
                PrivMsg(channel,message.ToString());
                
            }
        }
        private void Message_Voice(string channel, string sender, string message)
        {
            if (settings.noobs[settings.noobs.IndexOf(sender)].level < 3)
            {
                PrivMsg(channel, "insufficient kredencials!");
            }
            else
            {
                String[] cmds = message.ToLower().Split(' ');
                if (cmds.Length <= 1)
                {
                    return;
                }
                else
                {
                    if (cmds[1].Equals(irc.Nickname, StringComparison.OrdinalIgnoreCase))
                    {
                        PrivMsg(channel, "aha");
                    }
                    else
                    {
                        irc.Voice(channel, cmds[1]);
                    }
                }
            }
        }
        private void Message_Devoice(string channel, string sender, string message)
        {
            if (settings.noobs[settings.noobs.IndexOf(sender)].level < 3)
            {
                PrivMsg(channel, "insufficient kredencials!");
            }
            else
            {
                String[] cmds = message.ToLower().Split(' ');
                if (cmds.Length <= 1)
                {
                    return;
                }
                else
                {
                    if (cmds[1].Equals(irc.Nickname, StringComparison.OrdinalIgnoreCase))
                    {
                        PrivMsg(channel, "aha");
                    }
                    else
                    {
                        irc.Devoice(channel, cmds[1]);
                    }
                }
            }
        }
    }
}