﻿using System;

namespace shalatik
{
    static class Colors
    {
        public static char NORMAL = (char)15;
        public static char BOLD = (char)2;
        public static char UNDERLINE = (char)31;
        public static char REVERSE = (char)22;
        public static string WHITE = String.Format("{0}00", (char)3);
        public static string BLACK = String.Format("{0}01", (char)3);
        public static string DARK_BLUE = String.Format("{0}02", (char)3);
        public static string DARK_GREEN = String.Format("{0}03", (char)3);
        public static string RED = String.Format("{0}04", (char)3);
        public static string BROWN = String.Format("{0}05", (char)3);
        public static string PURPLE = String.Format("{0}06", (char)3);
        public static string OLIVE = String.Format("{0}07", (char)3);
        public static string YELLOW = String.Format("{0}08", (char)3);
        public static string GREEN = String.Format("{0}09", (char)3);
        public static string TEAL = String.Format("{0}10", (char)3);
        public static string CYAN = String.Format("{0}11", (char)3);
        public static string BLUE = String.Format("{0}12", (char)3);
        public static string MAGENTA = String.Format("{0}13", (char)3);
        public static string DARK_GRAY = String.Format("{0}14", (char)3);
        public static string LIGHT_GRAY = String.Format("{0}15", (char)3);                                                                                                                                                   
    }
}
