﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Data.SQLite;

namespace shalatik
{
    class Settings
    {
        public Settings(string file)
        {
            JavaProperties config = new JavaProperties();
            StreamReader sr=null;
            try
            {
                sr = new StreamReader(file);
                config.Load(sr.BaseStream);
            }
            catch
            {
                GenerateConfig(file);
                sr = new StreamReader(file);
                config.Load(sr.BaseStream);
            }
            try
            {
                LoadConfig(config);
            }
            catch
            {
                sr.Close();
                GenerateConfig(file);
                sr = new StreamReader(file);
                config.Load(sr.BaseStream);
                LoadConfig(config);
            }
            sr.Close();
            LoadQogWords();
            LoadSpells();
            noobs = new List<Noob>();
            userlist = LoadAuthUsers();
            Console.WriteLine("connecting to db");
            string MyConString = "Data Source=shalatik.db;Version=3;UseUTF16Encoding=True;";
            string MyConStringRO = "Data Source=shalatik.db;Version=3;UseUTF16Encoding=True;Read Only=True;";
            NaplnCommandlist();
            con = new SQLiteConnection(MyConString);
            con.Open();
            /*con2 = new SQLiteConnection(MyConString);
            con2.Open();*/
        }

        private void NaplnCommandlist()
        {
            commandlist = new string[11];
            commandlist[0] = "level 0: !help, !register, !login, !level, !addquote, !quote, !quotefrom, !ping, !uptime, !bing, !google, !addgreet, !qog, !qogstats, !roll, !rose, !logout, ??, myinfo";
            commandlist[1] = "level 1: !learn, !voiceme, !pass (query), !logout (query)";
            commandlist[2] = "level 2: !id, !ver";
            commandlist[3] = "level 3: !voice, !devoice";
            commandlist[4] = "";
            commandlist[5] = "level 5: !kick, !opme, !op, !deop, !say (query), !ignore, !unignore";
            commandlist[6] = "";
            commandlist[7] = "level 7: !learned, !unlearn";
            commandlist[8] = "";
            commandlist[9] = "";
            commandlist[10] = "level 10: !setlevel";
        }

        private static Dictionary<string, string> LoadAuthUsers()
        {
            Dictionary<string, string> users = new Dictionary<string, string>();
            StreamReader sr;
            try
            {        
                sr = new StreamReader("users.dat");
                while (!sr.EndOfStream)
                {
                    string[] text = sr.ReadLine().Split(' ');
                    users.Add(text[0], String.Format("{0} {1}", text[1], text[2]));
                }
                sr.Close();
                sr.Dispose();

            }
            catch
            {
                users = new Dictionary<string, string>();
            }
            return users;

        }
        internal void SaveAuthUsers()
        {
            StreamWriter sw = new StreamWriter("users.dat");
            foreach (string key in userlist.Keys)
            {
                sw.WriteLine(String.Format("{0} {1}", key, userlist[key]));
            }
            sw.Close();
            sw.Dispose();

        }

        private void LoadConfig(JavaProperties config)
        {
            server = new Queue<string>();
            CultureInfo ci = new CultureInfo("en-us");
            foreach (string str in config.GetProperty("server").Split(' '))
            {
                server.Enqueue(str);
            }
            chan = config.GetProperty("channel").Split(' ');
            nick = config.GetProperty("nick");

            db = config.GetProperty("db");


            if (db == null ) throw new Exception("pojeeebaliii koneee voooz!!!");

            strhpkoef = float.Parse(config.GetProperty("strhpkoef"), ci);
            strmanakoef = float.Parse(config.GetProperty("strmanakoef"), ci);
            inthpkoef = float.Parse(config.GetProperty("inthpkoef"), ci);
            intmanakoef = float.Parse(config.GetProperty("intmanakoef"), ci);
            agihpkoef = float.Parse(config.GetProperty("agihpkoef"), ci);
            agimanakoef = float.Parse(config.GetProperty("agimanakoef"), ci);
            hpkoef = float.Parse(config.GetProperty("hpkoef"), ci);
            manakoef = float.Parse(config.GetProperty("manakoef"), ci);
            staminakoef = float.Parse(config.GetProperty("staminakoef"), ci);
            strstaminakoef = float.Parse(config.GetProperty("strstaminakoef"), ci);
            intstaminakoef = float.Parse(config.GetProperty("intstaminakoef"), ci);
            agistaminakoef = float.Parse(config.GetProperty("agistaminakoef"), ci);
            attackcd = Int32.Parse(config.GetProperty("attackcd"), ci);

            strbasehp = float.Parse(config.GetProperty("strbasehp"), ci);
            intbasehp = float.Parse(config.GetProperty("intbasehp"), ci);
            agibasehp = float.Parse(config.GetProperty("agibasehp"), ci);
            strbasemana = float.Parse(config.GetProperty("strbasemana"), ci);
            intbasemana = float.Parse(config.GetProperty("intbasemana"), ci);
            agibasemana = float.Parse(config.GetProperty("agibasemana"), ci);
            strbasestamina = float.Parse(config.GetProperty("strbasestamina"), ci);
            intbasestamina = float.Parse(config.GetProperty("intbasestamina"), ci);
            agibasestamina = float.Parse(config.GetProperty("agibasestamina"), ci);

            stamina_attack = float.Parse(config.GetProperty("stamina_attack"), ci);

            respawninterval = int.Parse(config
                            .GetProperty("respawninterval"), ci);
            port = int.Parse(config
                            .GetProperty("port"));
            regeninterval = int.Parse(config
                            .GetProperty("regeninterval"), ci);
        }
        public Settings()
        {
            
        }

        static void GenerateConfig(string file)
        {
            using (StreamWriter content = new StreamWriter(file))
            {
                content.WriteLine("# server settings");
                content.WriteLine("");
                content.WriteLine("server = irc.gts.cz");
                content.WriteLine("port = 6667");
                content.WriteLine("channel = #fiit");
                content.WriteLine("nick = shalatik");
                content.WriteLine("db = shalatik.db");
                content.WriteLine("");
                content.WriteLine("");
                content.WriteLine("# koeficienty");
                content.WriteLine("");
                content.WriteLine("strhpkoef = 3");
                content.WriteLine("strmanakoef = 1.2");
                content.WriteLine("inthpkoef = 1.5");
                content.WriteLine("intmanakoef = 3.3");
                content.WriteLine("agihpkoef = 2");
                content.WriteLine("agimanakoef = 2");
                content.WriteLine("agistaminakoef = 2");
                content.WriteLine("strstaminakoef = 1.2");
                content.WriteLine("intstaminakoef = 1.2");
                content.WriteLine("stamina_attack = 20");
                content.WriteLine("strbasehp = 200");
                content.WriteLine("intbasehp = 150");
                content.WriteLine("agibasehp = 175");
                content.WriteLine("strbasemana = 100");
                content.WriteLine("intbasemana = 200");
                content.WriteLine("agibasemana = 125");
                content.WriteLine("strbasestamina = 50");
                content.WriteLine("intbasestamina = 50");
                content.WriteLine("agibasestamina = 100");
                content.WriteLine("hpkoef = 17");
                content.WriteLine("manakoef = 13");
                content.WriteLine("staminakoef = 15");
                content.WriteLine("attackcd = 10000");
                content.WriteLine("");
                content.WriteLine("# intervaly (v milisekundach 1 s = 1000 ms)");
                content.WriteLine("");
                content.WriteLine("respawninterval = 60000");
                content.WriteLine("regeninterval = 5000");
                content.Close();
            }
        }

        public void LoadQogWords()
        {
            List<String> line = new List<string>();
            StreamReader di = null;
            try
            {
                di = new StreamReader("qogconfig.cfg");
            }
            catch
            {
                line.Add("bluglafnuf 0.0");
                qogwords = line.ToArray();
                return;
            }  
            while (!di.EndOfStream)
            {
                line.Add(di.ReadLine());              
            }
            qogwords = line.ToArray();
            di.Close();
        }

        public void LoadSpells()
        {
            List<Spell> spells = new List<Spell>();
            try
            {
                using (FileStream fstream = new FileStream("Spells.xml", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    XmlDocument spellz = new XmlDocument();
                    spellz.Load(fstream);
                    XmlNodeList NodeList = spellz.GetElementsByTagName("Spell");
                    foreach (XmlNode node in NodeList)
                        spells.Add(new Spell(node));
                }
                this.spells = spells.ToArray();
            }
            catch
            {
                spells.Clear();
                spells.Add(new Spell());
                this.spells=spells.ToArray();
            }
        }
         

        public string[] chan { get; set; }

        public Spell[] spells { get; set; }

        public string[] qogwords { get; set; }

        public string[] commandlist { get; set; }

        public string dbserver { get; set; }

        public string dbuser { get; set; }

        public string dbpass { get; set; }

        public string db { get; set; }

        public string nick { get; set; }

        public SQLiteConnection con { get; set; }

        public SQLiteConnection con2 { get; set; }

        public List<Noob> noobs { get; set; }
        public Dictionary<string, string> userlist { get; set; }

        public float strhpkoef { get; set; }

        public float strbasehp { get; set; }
        public float intbasehp { get; set; }
        public float agibasehp { get; set; }
        public float strbasemana { get; set; }
        public float intbasemana { get; set; }
        public float agibasemana { get; set; }
        public float strbasestamina { get; set; }
        public float intbasestamina { get; set; }
        public float agibasestamina { get; set; }

        public float strmanakoef { get; set; }

        public float inthpkoef { get; set; }

        public float intmanakoef { get; set; }

        public float agihpkoef { get; set; }

        public float agimanakoef { get; set; }

        public float hpkoef { get; set; }

        public float manakoef { get; set; }

        public float staminakoef { get; set; }

        public float strstaminakoef { get; set; }

        public float intstaminakoef { get; set; }

        public float agistaminakoef { get; set; }

        public float stamina_attack { get; set; }

        public int attackcd { get; set; }

        public Queue<string> server { get; set; }

        public int port { get; set; }
        public int respawninterval { get; set; }
        public int regeninterval { get; set; }



        public void ResetDB12()
        {
            con.Close();
            con2.Close();
            string MyConString = String.Format("SERVER={0};DATABASE={1};UID={2};PASSWORD={3};", dbserver, db, dbuser, dbpass);
            con = new SQLiteConnection(MyConString);
            con.Open();
            con2 = new SQLiteConnection(MyConString);
            con2.Open();
        }
    }
}
