﻿using System;

namespace shalatik
{
    public class Noob
    {
        public string name;
        public int cooldown;
        public bool danced;
        public int attackcd;
        public DateTime lastqog;
        public float qogpend;
        public static implicit operator string(Noob n)
        {
            return n.name;
        }
        public static implicit operator Noob(string str)
        {
            return new Noob(str);
        }
        public Noob(string name)
        {
            this.name = name;
            cooldown = 0;
            attackcd = 0;
            danced = false;
            sucked = false;
            level = 0;
            qogpend = 0;
            lastqog = DateTime.Now.AddDays(-1);
        }
        public override string ToString()
        {
            return name;
        }
        public override bool Equals(object obj)
        {
            if (string.Compare(obj.ToString(), name, true) == 0) return true;
            else return false;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public bool sucked { get; set; }

        public int level { get; set; }
    }
}
